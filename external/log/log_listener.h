#pragma once

#include <cstdlib>
#include <iostream>

#include "net.h"

class log_listener
{
public:
	log_listener(const char* client_name, net::helpers::net_sender& sender);
	~log_listener();

	void write(const std::string& msg);

	const std::string& get_client_name() const {return client_name_;}

private:
	net::helpers::net_sender& m_sender;
	std::string client_name_;
};