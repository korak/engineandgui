#pragma once

#include <string>

namespace details
{
	struct string_packer;

	struct packer_holder	
	{
		packer_holder(string_packer* pt);
		packer_holder(const packer_holder& h);
		~packer_holder();

		packer_holder operator<<(const char* str);
		packer_holder operator<<(const std::string& str);
		packer_holder operator<< (int value);
		packer_holder write(const char* data, size_t size);

#ifdef LOG_UNICODE
		details::packer_holder operator<< (const wchar_t* str);
		details::packer_holder operator<< (const std::wstring& str);
		details::packer_holder write(const wchar_t* data, size_t size);
#endif

	private:
		string_packer* m_ptr;
	};
}

struct logger
{
	logger(bool raw_mode = false)
		: m_raw_mode(raw_mode), m_cur_message(0)
	{}

	static void init(const char* client_name, int server_port, 	
		const char* server_host = "127.0.0.1");

	static bool enable();
	static void enable(bool flag);

	details::packer_holder operator<< (const char* str);
	details::packer_holder operator<< (const std::string& str);
	details::packer_holder operator<< (int value);
	details::packer_holder write(const char* data, size_t size);

#ifdef LOG_UNICODE
	details::packer_holder operator<< (const wchar_t* str);
	details::packer_holder operator<< (const std::wstring& str);
	details::packer_holder write(const wchar_t* data, size_t size);
#endif

private:
	details::string_packer* create_packer();

private:
	static bool ms_enable;
	unsigned int m_cur_message;
	bool m_raw_mode;
	const char* m_prefix;
};


extern logger msg;
extern logger note;
extern logger wrn;
extern logger err;
extern logger fatal_error;
extern logger raw;