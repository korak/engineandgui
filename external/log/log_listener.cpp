#include "log_listener.h"

//#include <boost/date_time.hpp>

log_listener::log_listener(const char* client_name, net::helpers::net_sender& sender)
						 : m_sender(sender),
						 client_name_(client_name)
{
}

log_listener::~log_listener()
{
	//std::cout << "Disconnected." << std::endl;
}

void log_listener::write(const std::string& msg)
{
	m_sender.send(msg.c_str(), msg.size());
}