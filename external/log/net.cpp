#include <windows.h>

#include <string>
#include <vector>

#include "net.h"

#include <assert.h>

#define SO_MAX_MSG_SIZE   0x2003      /* maximum message size */

namespace net
{
	net_exception::net_exception(const char* msg)
	: message( msg ) 
	{
	}

	net_exception::net_exception(const std::string& msg) 
	: message( msg.c_str() ) 
	{
	}

	char* getErrorText(int err_code)
	{
		switch(err_code)
		{
		case WSANOTINITIALISED: return "A successful WSAStartup call must occur before using this function.";
		case WSAENETDOWN: return "The network subsystem has failed.";
		case WSAEFAULT: return "The buf or from parameters are not part of the user address space, or the fromlen parameter is too small to accommodate the peer address.";
		case WSAEINTR: return "The (blocking) call was canceled through WSACancelBlockingCall.";
		case WSAEINPROGRESS: return "A blocking Windows Sockets 1.1 call is in progress, or the service provider is still processing a callback function.";
		case WSAEINVAL: return "The socket has not been bound with bind, or an unknown flag was specified, or MSG_OOB was specified for a socket with SO_OOBINLINE enabled, or (for byte stream-style sockets only) len was zero or negative.";
		case WSAEISCONN: return "The socket is connected. This function is not permitted with a connected socket, whether the socket is connection-oriented or connectionless.";
		case WSAENETRESET: return "The connection has been broken due to the keep-alive activity detecting a failure while the operation was in progress.";
		case WSAENOTSOCK: return "The descriptor is not a socket.";
		case WSAEOPNOTSUPP: return "MSG_OOB was specified, but the socket is not stream-style such as type SOCK_STREAM, OOB data is not supported in the communication domain associated with this socket, or the socket is unidirectional and supports only send operations.";
		case WSAESHUTDOWN: return "The socket has been shut down; it is not possible to recvfrom on a socket after shutdown has been invoked with how set to SD_RECEIVE or SD_BOTH.";
		case WSAEWOULDBLOCK: return "The socket is marked as nonblocking and the recvfrom operation would block.";
		case WSAEMSGSIZE: return "The message was too large to fit into the specified buffer and was truncated.";
		case WSAETIMEDOUT: return "The connection has been dropped, because of a network failure or because the system on the other end went down without notice.";
		case WSAECONNRESET: return "The virtual circuit was reset by the remote side executing a hard or abortive close. The application should close the socket as it is no longer usable. On a UPD-datagram socket this error would indicate that a previous send operation resulted in an ICMP \"Port Unreachable\" message.";
		}
		return NULL;
	}

	net_exception::net_exception(int last_err) 
	{
		char* lpMsgBuf = getErrorText(last_err);

		if(last_err && !lpMsgBuf)
		{
			FormatMessageA( 
				FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
				NULL,
				last_err,
				0, // Default language
				(LPSTR) &lpMsgBuf,
				0,
				NULL 
				);
			// Process any inserts in lpMsgBuf.
			message = lpMsgBuf;

			// Free the buffer.
			LocalFree( lpMsgBuf );
		}
		else
		{
			message = lpMsgBuf ? lpMsgBuf : "Unknown error!";
		}
	}

	//////////////////////////////////////////////////////////////////////////

	address::address(const std::string& host, word port)
	{
		m_port = port;
		u_long res = inet_addr( host.c_str() );
		if(res != INADDR_NONE)
			m_host = ntohl( res );
		else
			throw net_exception(WSAGetLastError());
	}
	
	std::string address::string() const
	{
		unsigned char  c_host[4];
		*(dword*)c_host = host();
		char	buffer[256];

		sprintf_s( buffer, "%d.%d.%d.%d",int(c_host[0]),int(c_host[1]),int(c_host[2]),int(c_host[3]) );
		return buffer;
	}

	//////////////////////////////////////////////////////////////////////////

	namespace
	{
		int socket_counter = 0;

		void	StartWinSock()
		{
			socket_counter++;
			if( socket_counter != 1 )return;

			WORD wVersionRequested = MAKEWORD( 2, 2 );
			WSADATA wsaData;	

			if (WSAStartup( wVersionRequested, &wsaData )) {
				//MessageBox(NULL,"WSAStartup failed","StartWinSock",MB_OK);
				return;
			}	

			if ( LOBYTE( wsaData.wVersion ) != 2 ||
				HIBYTE( wsaData.wVersion ) != 2 ) {
					//MessageBox(NULL,"WinSock wrong version","StartWinSock",MB_OK);
					WSACleanup();
					return; 
			}	
		}

		void StoptWinSock() {
			socket_counter--;
			if( socket_counter != 0 )return;

			if(int res = WSACleanup()){
				//MessageBox(NULL,"WSACleanup() failed","StoptWinSock",MB_OK);
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////



	socket::socket(const uint_ptr new_s )
	{
		StartWinSock();
		m_socket = new_s;
	}

	socket::socket(int af,int type,int protocol)
	{
		StartWinSock();

		assert(sizeof(m_socket) == sizeof(SOCKET));
		m_socket = (uint_ptr)::socket(af, type, protocol);
		if ( m_socket == INVALID_SOCKET ) 
		{
			assert(false && "socket::socket() : m_socket == INVALID_SOCKET");
			throw net_exception(WSAGetLastError());
		}
	}

	int	socket::listen(int backlog)
	{
		if( ::listen( m_socket, backlog  ) == SOCKET_ERROR )
		{
			assert(false && "socket::listen() : ::listen( m_socket, backlog  ) == SOCKET_ERROR ");
			throw net_exception(WSAGetLastError());
		}

		return 0;
	}

	uint_ptr	socket::accept( address& addr )
	{
		struct sockaddr_in in_addr;
		int addrlen =  sizeof( in_addr );

		SOCKET new_s = ::accept(m_socket, (LPSOCKADDR)&in_addr, &addrlen );
		if( new_s  == INVALID_SOCKET )
		{
			assert(false && "socket::accept : new_s == INVALID_SOCKET ");
			throw net_exception(WSAGetLastError());		
		}

		addr = net::address( in_addr.sin_addr.s_addr, in_addr.sin_port );

		return (uint_ptr)new_s;
	}

	int	socket::bind(address& addr)
	{
		struct sockaddr_in addr_in;
		addr_in.sin_family = AF_INET; // should be the same as has constructor
		addr_in.sin_port = htons( addr.port() );
		addr_in.sin_addr.s_addr = htonl( addr.host() );

		if(::bind(m_socket, (LPSOCKADDR)&addr_in, sizeof(addr_in)) == INVALID_SOCKET)
		{
			assert(false && "socket::bind : (LPSOCKADDR)&addr_in, sizeof(addr_in)) == INVALID_SOCKET");
			throw net_exception(WSAGetLastError());
		}

		return 0;
	}

	address	socket::getaddress()
	{
		struct sockaddr_in addr_in;
		int length = sizeof(addr_in);

		if(getsockname(m_socket,(LPSOCKADDR)&addr_in,&length )==SOCKET_ERROR)
		{
			assert(false && "socket::getaddress : getsockname(m_socket,(LPSOCKADDR)&addr_in,&length )==SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}

		return address(ntohl(addr_in.sin_addr.s_addr), ntohs(addr_in.sin_port));
	}

	address	socket::getpeeraddress()
	{
		struct sockaddr_in addr_in;
		int length = sizeof(addr_in);	

		if(getpeername(m_socket,(LPSOCKADDR)&addr_in,&length))
		{
			assert(false && "socket::getpeeraddress : getpeername(m_socket,(LPSOCKADDR)&addr_in,&length)");
			throw net_exception(WSAGetLastError());
		}
		return address(ntohl(addr_in.sin_addr.s_addr), ntohs(addr_in.sin_port));
	}

	int	socket::connect(address& addr)
	{
		struct sockaddr_in addr_in;
		addr_in.sin_family = AF_INET; // should be the same as has constructor
		addr_in.sin_port = htons( addr.port() );
		addr_in.sin_addr.s_addr = htonl( addr.host() );

		int result = ::connect( m_socket, (LPSOCKADDR)&addr_in, sizeof(addr_in) );
		if( result == SOCKET_ERROR)
		{
			assert(false && "socket::connect : result == SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}

		return result;
	}

	int	socket::send(const std::vector< char >& data)
	{
		int result = ::send(m_socket, &data.front(), (int)data.size(), MSG_DONTROUTE );

		if( result == SOCKET_ERROR)
		{
			assert(false && "socket::send : result == SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}

		return result;
	}

	int	socket::sendto(address& addr,const std::vector< char >& data)
	{
		struct sockaddr_in addr_in;
		addr_in.sin_family = AF_INET; // should be the same as has constructor
		addr_in.sin_port = htons( addr.port() );
		addr_in.sin_addr.s_addr = htonl( addr.host() );

		int	sent_number = ::sendto(m_socket,&data.front(),(int)data.size(),NULL,(LPSOCKADDR)&addr_in, sizeof(addr_in));
		if(sent_number == SOCKET_ERROR)
		{
			assert(false && "sendto::send : sent_number == SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}

		return sent_number;
	}

	int	socket::receive(std::vector< char >& data, int flags)
	{
		u_long length=0;

		if( ::ioctlsocket( m_socket, FIONREAD, &length ) == SOCKET_ERROR )
		{
			assert(false && "sendto::receive : ::ioctlsocket( m_socket, FIONREAD, &length ) == SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}

		char*	buffer = new char[ length ];
		int res = recv( m_socket, buffer, length, flags );
		if( res == SOCKET_ERROR)
		{
			assert(false && "sendto::receive : res == SOCKET_ERROR");
			delete buffer;			
			throw net_exception(WSAGetLastError());
		}

		data.resize(res);
		memcpy(&data.front(),buffer,res);
		delete	buffer;
		return (int)data.size();
	}

	int	socket::receivefrom(address& addr,std::vector< char >& data, int flags)
	{
		int length = getopt(SOL_SOCKET,SO_MAX_MSG_SIZE);	

		char*	buffer = new char[length];

		sockaddr_in addr_from;
		int	addr_from_size = sizeof(addr_from);

		int res = recvfrom(m_socket, buffer,length,flags,(sockaddr*)&addr_from,&addr_from_size);
		if( res == SOCKET_ERROR)
		{
			assert(false && "sendto::receivefrom : res == SOCKET_ERROR");
			delete buffer;
			throw net_exception(WSAGetLastError());				
		}
		addr = net::address( ntohl(addr_from.sin_addr.s_addr),ntohs(addr_from.sin_port) );

		data.resize(res);
		memcpy(&data.front(),buffer,res);
		delete	buffer;
		return (int)data.size();
	}

	int	socket::canread( int timeout )
	{
		// timeout - miliseconds
		fd_set	set;
		set.fd_count = 1;
		set.fd_array[0] = m_socket;
		timeval	t_out = {timeout/1000,timeout * 10};
		int res = select(NULL,&set,NULL,NULL,&t_out);
		
		if(res == SOCKET_ERROR)
		{
			assert(false && "sendto::canread : res == SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}

		if(res == 1 ) return true;
		return false;
	}

	int	socket::canwrite(int timeout)
	{
		fd_set	set;
		set.fd_count = 1;
		set.fd_array[0] = m_socket;
		timeval	t_out = {timeout/1000, timeout * 10};	
		int res = select(NULL,NULL,&set,NULL,&t_out);
		if(res == SOCKET_ERROR)
		{
			assert(false && "sendto::canwrite : res == SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}
		if(res == 1) return true;
		return false;
	}

	int	socket::getopt(int level,int optname)
	{
		int	value;
		int	length = sizeof(int);

		if(getsockopt (m_socket,level,optname,(char*)&value,&length)==SOCKET_ERROR)
		{
			assert(false && "sendto::getopt : getsockopt (m_socket,level,optname,(char*)&value,&length)==SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}

		return value;
	}

	int	socket::setopt(int level,int optname,const char* value, size_t v_len)
	{
		int result = setsockopt (m_socket,level,optname, value, (int)v_len);

		if( result == SOCKET_ERROR )
		{
			assert(false && "sendto::setopt : result == SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}

		return result;
	}

	int	socket::setopt(int level,int optname,int value)
	{
		int result = setsockopt (m_socket,level,optname,(char*)&value,sizeof(int));

		if( result == SOCKET_ERROR )
		{
			assert(false && "sendto::setopt : result == SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}

		return result;
	}

	int socket::shutdown( int how )
	{
		int result = ::shutdown( m_socket, how );

		if( result == SOCKET_ERROR )
		{
			assert(false && "sendto::shutdown : result == SOCKET_ERROR");
			throw net_exception(WSAGetLastError());
		}

		return result;
	}


	int socket::close( )
	{
		if( !m_socket || m_socket == SOCKET_ERROR) return 0;

		return closesocket(m_socket);
	}

	int socket::graceful_shutdown ( )
	{
		const	int	RECEIVE_TIMEOUT	= 1000;	// milliseconds
		const	int	SEND_TIMEOUT	= 1;	// seconds

		shutdown( HL_SD_SEND );

		DWORD StartTimeout = GetTickCount();
		while( true )
		{
			if( canread( 10 ) && ( receive( std::vector<char>() ) == 0 ) )
				break;

			if( GetTickCount() - StartTimeout >= RECEIVE_TIMEOUT )
			{
				assert(false && "graceful_shutdown::shutdown : GetTickCount() - StartTimeout >= RECEIVE_TIMEOUT");
				throw "Client has not closed socket within 1 sec";
			}
		}

		// 2 sec for data to be sent before closesocket
		linger	lin = { 1, SEND_TIMEOUT };
		setopt( SOL_SOCKET, SO_LINGER, (char*)&lin, sizeof(lin) );

		close();
	}

	socket::~socket()
	{
		close();
		StoptWinSock();
	}

	//////////////////////////////////////////////////////////////////////////

	namespace helpers
	{
		net_sender::net_sender(const std::string& addr, int port)
			: m_socket(AF_INET,SOCK_DGRAM,0)
			, m_addr(addr,port)
		{
			m_socket.setopt(SOL_SOCKET,SO_BROADCAST,TRUE);
		}

		void net_sender::send(const char* ptr, size_t size)
		{
			std::vector<char> data(ptr,ptr+size);
			m_socket.sendto( m_addr, data );
		}

		//////////////////////////////////////////////////////////////////////////

		net_receiver::net_receiver(const std::string& addr, int port)
			: m_socket(AF_INET,SOCK_DGRAM,0),m_addr(addr,port)
		{
			m_socket.setopt(SOL_SOCKET,SO_BROADCAST,TRUE);
			m_socket.bind(net::address(INADDR_ANY, port) );
		}

		std::vector<char> net_receiver::receive(int timeout)
		{
			std::vector<char> data;
			if( m_socket.canread(timeout) )
			{
				m_socket.receivefrom( m_addr, data );
			}
			return data;
		}
	}
}
