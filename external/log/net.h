#pragma once

#include <string>
#include <vector>


#pragma comment(lib,"Ws2_32.lib")

namespace net
{
	typedef unsigned char	byte;
	typedef unsigned short	word;
	typedef unsigned int	dword;
	typedef unsigned int*	dwordp;

#ifdef _WIN64
	typedef unsigned __int64 uint_ptr;
#else
	typedef unsigned int uint_ptr;
#endif

	class net_exception : public std::exception
	{
	public:
		explicit net_exception(const char* msg);
		explicit net_exception(const std::string& msg);
		explicit net_exception(int last_err);

	private:		
		const char *what() const { return message.c_str(); }

		std::string	message;
	};

	class address
	{	
	public:
		address(dword host = 0,word port = 0):m_host(host), m_port(port){}
		address(const std::string& host, word port);	

		std::string string() const;
		inline word	port()const {return m_port;}
		inline dword host()const {return m_host;}

	private:
		dword	m_host;
		word	m_port;
	};

	enum
	{
		// Maximum queue length specifiable by listen.
		HL_SOMAXCONN  = 0x7fffffff,
		HL_SD_RECEIVE = 0x00,
		HL_SD_SEND    = 0x01,
		HL_SD_BOTH    = 0x02
	};

	class socket
	{
	public:
		explicit socket(const uint_ptr ); // SOCKET	

		// AF_INET, SOCK_DGRAM, 0
		socket(int af,int type,int protocol);

		virtual	int	bind(address& addr);
		virtual	int	listen(int backlog = HL_SOMAXCONN );

		virtual	uint_ptr accept(address& addr = address());

		virtual	address	getaddress();
		virtual	address	getpeeraddress();

		virtual	int	connect(address& addr);
		virtual	int	send(const std::vector< char >& data);
		virtual	int	sendto(address& addr, const std::vector< char >& data);

		virtual	int	receive(std::vector< char >& data, int flags = 0 );
		virtual	int	receivefrom(address& addr,std::vector< char >& data, int flags = 0);

		virtual	int	canread(int timeout = 0); //milliseconds
		virtual	int	canwrite(int timeout = 0);//milliseconds

		virtual	int	getopt(int level,int optname);	
		virtual	int	setopt(int level,int optname,int value);
		virtual	int	setopt(int level,int optname,const char* value, size_t v_len);

		virtual	int shutdown( int how );
		virtual	int close( );
		virtual	int graceful_shutdown ( );

		virtual	~socket();

	private:
		uint_ptr m_socket;// SOCKET

		socket(const socket&);
		socket& operator=(socket&);
	};

	namespace helpers
	{
		class net_sender
		{
			net::socket m_socket;
			net::address m_addr;
		public:
			net_sender(const std::string& addr, int port);
			void send(const char* ptr, size_t size);
		};

		class net_receiver
		{
			net::socket m_socket;
			net::address m_addr;
		public:
			net_receiver(const std::string& addr, int port);

			std::vector<char> receive(int timeout=0);
		};
	}
}