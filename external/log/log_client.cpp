#include "log_client.h"

#include "log_listener.h"

#ifdef LOG_TIME
#include <boost/lexical_cast.hpp>
#include <boost/date_time.hpp>
#endif

#ifdef LOG_UNICODE
	#include <utf/utf8.h>
#endif

static log_listener* chat_client_ptr;

//////////////////////////////////////////////////////////////////////////
namespace details
{
	struct string_packer
	{
		string_packer(const char* prefix, int message_index, bool raw = false) : counter(0)
		{
			if (raw) return;

			char buff[256];

			#ifdef LOG_TIME
				//using namespace boost::posix_time;

				//ptime now = second_clock::local_time();

				//int hours = now.time_of_day().hours();
				//int minutes = now.time_of_day().minutes();
				//int seconds = now.time_of_day().seconds();

			
				//sprintf_s(buff, "%d:%d:%d|", hours, minutes, seconds);

				//m_text_buffer += buff;
				//m_text_buffer += " ";

				//sprintf_s(buff, "%d|", message_index);
				//m_text_buffer += buff;

				//if (chat_client_ptr)
				//	m_text_buffer += chat_client_ptr->get_client_name() + "|";
			#endif
			if (prefix)
				m_text_buffer += prefix;
		}

		~string_packer()
		{
			if (chat_client_ptr)
			{
				chat_client_ptr->write(m_text_buffer);
			}
		}

		void operator() (const char* str)
		{
			m_text_buffer += str;
		}

		void operator() (int value)
		{
			char buff[256];
			sprintf_s(buff, "%i", value);
			//std::string str = boost::lexical_cast<std::string>(value);
			m_text_buffer += buff;
		}

		void operator() (const std::string& str)
		{
			m_text_buffer += str;
		}		

		void write(const char* data, size_t size)
		{
			m_text_buffer.append(data, size);
		}

		void addref() {++counter;}
		void release()
		{
			--counter;
			if (counter < 1)
			{
				delete this;
			}
		}

	private:
		std::string m_text_buffer;
		int counter;
	};
	//////////////////////////////////////////////////////////////////////////
	packer_holder::packer_holder(string_packer* pt)	: m_ptr(pt) 
	{
		if (m_ptr) m_ptr->addref();
	}

	packer_holder::packer_holder(const packer_holder& h)
		:m_ptr(h.m_ptr)
	{
		if (m_ptr) m_ptr->addref();
	}

	packer_holder::~packer_holder()
	{
		if (m_ptr) m_ptr->release();
	}

	packer_holder packer_holder::operator<<(const char* str)
	{
		if (m_ptr) (*m_ptr)(str);
		return *this;
	}

	packer_holder packer_holder::operator<<(const std::string& str)
	{
		if (m_ptr) (*m_ptr)(str);
		return *this;
	}

	packer_holder packer_holder::operator<< (int value)
	{
		if (m_ptr)  (*m_ptr)(value);
		return *this;
	}

	packer_holder packer_holder::write(const char* data, size_t size)
	{
		if (m_ptr && data) m_ptr->write(data, size);
		return *this;
	}
#ifdef LOG_UNICODE
	packer_holder packer_holder::operator<< (const wchar_t* str)
	{
		if (m_ptr) {
			std::wstring utf16(str);
			std::string utf8 = utf8::utf16to8 (utf16);

			(*m_ptr)(utf8.c_str());
		}

		return *this;
	}

	packer_holder packer_holder::operator<< (const std::wstring& str)
	{
		if (m_ptr) {
			const std::wstring& utf16 = str;
			std::string utf8 = utf8::utf16to8 (utf16);
			(*m_ptr)(utf8);
		}

		return *this;
	}

	packer_holder packer_holder::write(const wchar_t* data, size_t size)
	{
		if (m_ptr && data){
			std::wstring utf16(data, size);
			std::string utf8 = utf8::utf16to8 (utf16);
			m_ptr->write(utf8.c_str(), utf8.size());
		}

		return *this;
	}
#endif
}
//////////////////////////////////////////////////////////////////////////

using details::packer_holder;
using details::string_packer;

bool logger::ms_enable;
bool logger::enable() {return ms_enable;}
void logger::enable(bool flag) {ms_enable = flag;}

string_packer* logger::create_packer()
{
	return ms_enable ? new string_packer(m_prefix, ++m_cur_message, m_raw_mode) : 0;
}

packer_holder logger::operator<< (const char* str)
{
	packer_holder h(create_packer());
	return h << str;
}

packer_holder logger::operator<< (const std::string& str)
{
	return (*this) << str.c_str();
}

packer_holder logger::operator<< (int value)
{
	packer_holder h(create_packer());
	return h << value;
}

details::packer_holder logger::write(const char* data, size_t size)
{
	packer_holder h(create_packer());
	return h.write(data, size);
}

#ifdef LOG_UNICODE
packer_holder logger::operator<< (const wchar_t* str)
{
	packer_holder h(create_packer());
	return h << str;
}

packer_holder logger::operator<< (const std::wstring& str)
{
	return (*this) << str.c_str();
}

packer_holder logger::write(const wchar_t* data, size_t size)
{
	packer_holder h(create_packer());
	return h.write(data, size);
}
#endif
//////////////////////////////////////////////////////////////////////////

logger msg;
logger note;
logger wrn;
logger err;
logger fatal_error;
logger raw(true);


#include <iostream>
//#include <boost/date_time/posix_time/posix_time.hpp>


struct chat_client_holder
{
	chat_client_holder()
		: m_chat_client_internal_ptr(0)
		, m_sender(0)
	{
	}

	~chat_client_holder()
	{		
		chat_client_ptr = 0;
		delete m_chat_client_internal_ptr;
		delete m_sender;
	}

	void create(const char* client_name, int server_port, const char* server_host)
	{		
		m_sender = new net::helpers::net_sender(server_host,server_port);
		m_chat_client_internal_ptr = new log_listener(client_name, *m_sender);		
		chat_client_ptr = m_chat_client_internal_ptr;
	}

private:
	log_listener* m_chat_client_internal_ptr;
	net::helpers::net_sender *m_sender;

} __holder;

void logger::init(const char* client_name, int server_port, const char* server_host)
{
	msg.m_prefix = "MSG: ";
	note.m_prefix = "NOTE: ";
	wrn.m_prefix = "WRN: ";
	err.m_prefix = "ERROR: ";
	raw.m_prefix = "";
	fatal_error.m_prefix = "FATAL ERROR: ";

	__holder.create(client_name, server_port, server_host);
}