#include "stdafx.h"

#include "IGizmo.h"

#include "GizmoTransform.h"
#include "GizmoTransformMove.h"
#include "GizmoTransformRotate.h"
#include "GizmoTransformScale.h"

#include "GizmoTransformRender.h"

#define FIC(func) for(size_t i = IGizmo::MOVE; i < IGizmo::MODE_SIZE; ++i) {gizmos[i]->func;}


typedef std::shared_ptr<IGizmoRenderer> RendererPtr;

class Gizmo : public IGizmo
{
	IGizmo::MODE mode;

	CGizmoTransform* gizmos[IGizmo::MODE_SIZE];

	CGizmoTransformMove move_gizmo;
	CGizmoTransformRotate rotate_gizmo;
	CGizmoTransformScale scale_gizmo;

	RendererPtr m_renderer;
public:
	explicit Gizmo(RendererPtr renderer) 
		: move_gizmo(renderer.get())
		, rotate_gizmo(renderer.get())
		, scale_gizmo(renderer.get())
		, m_renderer(renderer)
	{
		mode = IGizmo::MOVE;
		gizmos[IGizmo::MOVE] = &move_gizmo;
		gizmos[IGizmo::ROTATION] = &rotate_gizmo;
		gizmos[IGizmo::SCALE] = &scale_gizmo;
	}

	~Gizmo() {
	}

	CGizmoTransform& p() {return *gizmos[mode];}

	void SetMode(IGizmo::MODE mode) {this->mode = mode;}
	IGizmo::MODE GetMode() {return mode;}
	
	void SetEditMatrix(float *m) {FIC(SetEditMatrix(m));}

	void SetCameraMatrix(const float *Model, const float *Proj) {FIC(SetCameraMatrix(Model, Proj));}
	void SetScreenDimension( int screenWidth, int screenHeight) {FIC(SetScreenDimension(screenWidth, screenHeight));}
	void SetDisplayScale( float aScale ) {FIC(SetDisplayScale(aScale));}

	bool OnMouseDown(unsigned int x, unsigned int y) {return p().OnMouseDown(x, y);}
	void OnMouseMove(unsigned int x, unsigned int y) {FIC(OnMouseMove(x, y));}
	void OnMouseUp(unsigned int x, unsigned int y) {FIC(OnMouseUp(x,y));}

	void UseSnap(bool bUseSnap) {FIC(UseSnap(bUseSnap));}
	bool IsUsingSnap() {return p().IsUsingSnap();}
	void SetSnap(float snapx, float snapy, float snapz) {FIC(SetSnap(snapx,snapy,snapz));}
	void SetSnap(const float snap) {FIC(SetSnap(snap));}

	void SetLocation(IGizmo::LOCATION aLocation)  {FIC(SetLocation(aLocation));}
	IGizmo::LOCATION GetLocation() {return p().GetLocation();}
	void SetAxisMask(unsigned int mask) {FIC(SetAxisMask(mask));}

	// rendering
	void Draw(const float *Model, const float *Proj) {
		p().SetCameraMatrix(Model, Proj);
		p().Draw();
	}
};

#ifdef MAC_OS
#import <OpenGL/OpenGL.h>
#else
#include <GL/gl.h>
#endif

#define V3(x) (*(GizmoRendererExt::vec3* const)(&x))
#define V4(x) (*(GizmoRendererExt::vec4* const)(&x))

struct CGizmomRenderExtWrap : public IGizmoRenderer
{
	typedef std::shared_ptr<GizmoRendererExt> ext_renderer_ptr_t;
	ext_renderer_ptr_t m_extRenderer;

	explicit CGizmomRenderExtWrap(ext_renderer_ptr_t ptr) : m_extRenderer(ptr) {}
	~CGizmomRenderExtWrap() {}

	void DrawCircle(const tvector3 &orig,float r,float g,float b,const tvector3 &vtx,const tvector3 &vty) {
		if (!m_extRenderer) return;
		m_extRenderer->DrawCircle(V3(orig), r, g, b, V3(vtx), V3(vty));
	}

	void DrawCircleHalf(const tvector3 &orig,float r,float g,float b,const tvector3 &vtx,const tvector3 &vty, const tplane &camPlan) {
		if (!m_extRenderer) return;
		m_extRenderer->DrawCircleHalf(V3(orig), r, g, b, V3(vtx), V3(vty), V4(camPlan));
	}

	void DrawAxis(const tvector3 &orig, const tvector3 &axis, const tvector3 &vtx,const tvector3 &vty, const tvector4 &col) {
		if (!m_extRenderer) return;
		m_extRenderer->DrawAxis(V3(orig), V3(axis), V3(vtx), V3(vty), V4(col));
	}

	void DrawCamem(const tvector3& orig,const tvector3& vtx,const tvector3& vty,float ng) {
		if (!m_extRenderer) return;
		m_extRenderer->DrawCamem(V3(orig), V3(vtx), V3(vty), ng);
	}

	void DrawQuad(const tvector3& orig, float size, bool bSelected, const tvector3& axisU, const tvector3 &axisV) {
		if (!m_extRenderer) return;
		m_extRenderer->DrawQuad(V3(orig), size, bSelected, V3(axisU), V3(axisV));
	}

	void DrawTri(const tvector3& orig, float size, bool bSelected, const tvector3& axisU, const tvector3& axisV) {
		if (!m_extRenderer) return;
		m_extRenderer->DrawTri(V3(orig), size, bSelected, V3(axisU), V3(axisV));
	}
};


struct CGizmoTransformRender : public IGizmoRenderer
{
	CGizmoTransformRender();
	void DrawCircle(const tvector3 &orig,float r,float g,float b,const tvector3 &vtx,const tvector3 &vty);
	void DrawCircleHalf(const tvector3 &orig,float r,float g,float b,const tvector3 &vtx,const tvector3 &vty, const tplane &camPlan);
	void DrawAxis(const tvector3 &orig, const tvector3 &axis, const tvector3 &vtx,const tvector3 &vty,const tvector4 &col);
	void DrawCamem(const tvector3& orig,const tvector3& vtx,const tvector3& vty,float ng);
	void DrawQuad(const tvector3& orig, float size, bool bSelected, const tvector3& axisU, const tvector3 &axisV);
	void DrawTri(const tvector3& orig, float size, bool bSelected, const tvector3& axisU, const tvector3& axisV);
};

CGizmoTransformRender::CGizmoTransformRender() {

}

void CGizmoTransformRender::DrawCircle(const tvector3 &orig,float r,float g,float b,const tvector3 &vtx,const tvector3 &vty)
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glColor4f(r,g,b,1);

	glBegin(GL_LINE_LOOP);
	for (int i = 0; i < 50 ; i++)
	{
		tvector3 vt;
		vt = vtx * cos((2*ZPI/50)*i);
		vt += vty * sin((2*ZPI/50)*i);
		vt += orig;
		glVertex3f(vt.x,vt.y,vt.z);
	}
	glEnd();
}


void CGizmoTransformRender::DrawCircleHalf(const tvector3 &orig,float r,float g,float b,const tvector3 &vtx,const tvector3 &vty, const tplane &camPlan)
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	

	glBegin(GL_LINE_STRIP);
	for (int i = 0; i < 30 ; i++)
	{
		
		tvector3 vt;
		vt = vtx * cos((ZPI/30)*i);
		vt += vty * sin((ZPI/30)*i);
		vt +=orig;
		if (camPlan.DotNormal(vt))
			glColor4f(r,g,b,1);
		else 
			glColor4f(r/2,g/2,b/2,1);

		glVertex3f(vt.x,vt.y,vt.z);
	}
	glEnd();
}

void CGizmoTransformRender::DrawAxis(const tvector3 &orig, const tvector3 &axis, const tvector3 &vtx,const tvector3 &vty, const tvector4 &col)
{
	const float fct = 0.05f;
	const float fct2 = 0.83f;

	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glColor4fv(&col.x);
	glBegin(GL_LINES);
	glVertex3fv(&orig.x);
	glVertex3f(orig.x+axis.x,orig.y+axis.y,orig.z+axis.z);
	glEnd();

	glBegin(GL_TRIANGLE_FAN);
	for (int i=0;i<=30;i++)
	{
		tvector3 pt;
		pt = vtx * cos(((2*ZPI)/30.0f)*i)*fct;
		pt+= vty * sin(((2*ZPI)/30.0f)*i)*fct;
		pt+=axis*fct2;
		pt+=orig;
		glVertex3fv(&pt.x);
		pt = vtx * cos(((2*ZPI)/30.0f)*(i+1))*fct;
		pt+= vty * sin(((2*ZPI)/30.0f)*(i+1))*fct;
		pt+=axis*fct2;
		pt+=orig;
		glVertex3fv(&pt.x);
		glVertex3f(orig.x+axis.x,orig.y+axis.y,orig.z+axis.z);

	}
	glEnd();

}

void CGizmoTransformRender::DrawCamem(const tvector3& orig,const tvector3& vtx,const tvector3& vty,float ng)
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	int i = 0 ;
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDisable(GL_CULL_FACE);


	glColor4f(1,1,0,0.5f);
	glBegin(GL_TRIANGLE_FAN);
	glVertex3fv(&orig.x);
	for (i = 0 ; i <= 50 ; i++)
	{
		tvector3 vt;
		vt = vtx * cos(((ng)/50)*i);
		vt += vty * sin(((ng)/50)*i);
		vt+=orig;
		glVertex3f(vt.x,vt.y,vt.z);
	}
	glEnd();

	glDisable(GL_BLEND);


	glColor4f(1,1,0.2f,1);
	tvector3 vt;
	glBegin(GL_LINE_LOOP);

	glVertex3fv(&orig.x);
	for ( i = 0 ; i <= 50 ; i++)
	{
		tvector3 vt;
		vt = vtx * cos(((ng)/50)*i);
		vt += vty * sin(((ng)/50)*i);
		vt+=orig;
		glVertex3f(vt.x,vt.y,vt.z);
	}

	glEnd();
}

void CGizmoTransformRender::DrawQuad(const tvector3& orig, float size, bool bSelected, const tvector3& axisU, const tvector3 &axisV)
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDisable(GL_CULL_FACE);

	tvector3 pts[4];
	pts[0] = orig;
	pts[1] = orig + (axisU * size);
	pts[2] = orig + (axisU + axisV)*size;
	pts[3] = orig + (axisV * size);

	if (!bSelected)
		glColor4f(1,1,0,0.5f);
	else
		glColor4f(1,1,1,0.6f);

	glBegin(GL_QUADS);
	glVertex3fv(&pts[0].x);
	glVertex3fv(&pts[1].x);
	glVertex3fv(&pts[2].x);
	glVertex3fv(&pts[3].x);
	glEnd();

	if (!bSelected)
		glColor4f(1,1,0.2f,1);
	else
		glColor4f(1,1,1,0.6f);

	glBegin(GL_LINE_STRIP);
	glVertex3fv(&pts[0].x);
	glVertex3fv(&pts[1].x);
	glVertex3fv(&pts[2].x);
	glVertex3fv(&pts[3].x);
	glEnd();

	glDisable(GL_BLEND);
}


void CGizmoTransformRender::DrawTri(const tvector3& orig, float size, bool bSelected, const tvector3& axisU, const tvector3& axisV)
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glDisable(GL_CULL_FACE);

	tvector3 pts[3];
	pts[0] = orig;

	pts[1] = (axisU );
	pts[2] = (axisV );

	pts[1]*=size;
	pts[2]*=size;
	pts[1]+=orig;
	pts[2]+=orig;

	if (!bSelected)
		glColor4f(1,1,0,0.5f);
	else
		glColor4f(1,1,1,0.6f);

	glBegin(GL_TRIANGLES);
	glVertex3fv(&pts[0].x);
	glVertex3fv(&pts[1].x);
	glVertex3fv(&pts[2].x);
	glVertex3fv(&pts[3].x);
	glEnd();

	if (!bSelected)
		glColor4f(1,1,0.2f,1);
	else
		glColor4f(1,1,1,0.6f);

	glBegin(GL_LINE_STRIP);
	glVertex3fv(&pts[0].x);
	glVertex3fv(&pts[1].x);
	glVertex3fv(&pts[2].x);
	glEnd();

	glDisable(GL_BLEND);
}

std::shared_ptr<IGizmo> CreateGizmo(std::shared_ptr<GizmoRendererExt> r) {
	
	if (r) {
		auto ptr = std::make_shared<CGizmomRenderExtWrap>(r);
		return std::make_shared<Gizmo>(ptr);
	}
	else {
		auto ptr = std::make_shared<CGizmoTransformRender>();
		return std::make_shared<Gizmo>(ptr);
	}	
}