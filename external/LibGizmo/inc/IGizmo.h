///////////////////////////////////////////////////////////////////////////////////////////////////
// LibGizmo
// File Name : IGizmo.h
// Creation : 10/01/2012
// Author : Cedric Guillemet
// Description : LibGizmo
//
///Copyright (C) 2012 Cedric Guillemet
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
//of the Software, and to permit persons to whom the Software is furnished to do
///so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
///FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
// 

#ifndef IGIZMO_H__
#define IGIZMO_H__

#include <memory>

struct IGizmo
{
	enum MODE {
		MOVE,
		ROTATION,
		SCALE,
		MODE_SIZE = 3
	};

    enum LOCATION {
        LOCATE_VIEW,
        LOCATE_WORLD,
        LOCATE_LOCAL,
    };

	enum ROTATE_AXIS {
		AXIS_X = 1,
		AXIS_Y = 2,
		AXIS_Z = 4,
		AXIS_TRACKBALL = 8,
		AXIS_SCREEN = 16,
		AXIS_ALL = 31
	};

	enum AXIS_MASK {
		MASK_AXIS_X = 1,
		MASK_AXIS_Y = 2,
		MASK_AXIS_Z = 4,
		MASK_AXIS_ALL = MASK_AXIS_X|MASK_AXIS_Y|MASK_AXIS_Z,
		MASK_AXIS_XY  = MASK_AXIS_X|MASK_AXIS_Y,
		MASK_AXIS_XZ  = MASK_AXIS_X|MASK_AXIS_Z,
		MASK_AXIS_YZ  = MASK_AXIS_Z|MASK_AXIS_Y,
		MASK_AXIS_NONE = 0
	};

	virtual void SetMode(MODE mode) {}
	virtual MODE GetMode() {return MOVE;}

	virtual void SetEditMatrix(float *pMatrix) = 0;
		
    virtual void SetScreenDimension( int screenWidth, int screenHeight) = 0;
    virtual void SetDisplayScale( float aScale ) = 0;

    // return true if gizmo transform capture mouse
	virtual bool OnMouseDown(unsigned int x, unsigned int y) = 0;
	virtual void OnMouseMove(unsigned int x, unsigned int y) = 0;
	virtual void OnMouseUp(unsigned int x, unsigned int y) = 0;

    // snaping
    virtual void UseSnap(bool bUseSnap) = 0;
	virtual bool IsUsingSnap() = 0;
    virtual void SetSnap(float snapx, float snapy, float snapz) = 0;
    virtual void SetSnap(const float snap) = 0;

    virtual void SetLocation(LOCATION aLocation)  = 0;
    virtual LOCATION GetLocation() = 0;
	virtual void SetAxisMask(unsigned int mask) = 0;

	virtual void Draw(const float *Model, const float *Proj) = 0;
};

struct GizmoRendererExt
{
	struct vec3 {float x, y, z;};
	struct vec4 {float x, y, z, w;};

	virtual void DrawCircle(const vec3 &orig,float r,float g,float b,const vec3 &vtx,const vec3 &vty) = 0;
	virtual void DrawCircleHalf(const vec3 &orig,float r,float g,float b,const vec3 &vtx,const vec3 &vty, const vec4 &camPlan) = 0;
	virtual void DrawAxis(const vec3 &orig, const vec3 &axis, const vec3 &vtx,const vec3 &vty, const vec4 &col) = 0;
	virtual void DrawCamem(const vec3& orig,const vec3& vtx,const vec3& vty,float ng) = 0;
	virtual void DrawQuad(const vec3& orig, float size, bool bSelected, const vec3& axisU, const vec3 &axisV) = 0;
	virtual void DrawTri(const vec3& orig, float size, bool bSelected, const vec3& axisU, const vec3& axisV) = 0;
};

std::shared_ptr<IGizmo> CreateGizmo(std::shared_ptr<GizmoRendererExt> r = std::shared_ptr<GizmoRendererExt>());

#endif
