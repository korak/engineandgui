#pragma once

#include <guilib/guilib.h>
#include <guilib/src/system.h>
#include <guilib/src/framewindow.h>

namespace gui {
	class Graph : public FrameWindow {
	public:
		typedef Graph self;
		typedef FrameWindow base;

		Graph(System& sys, const std::string& name = std::string());
		virtual ~Graph();

		void render(const gui::Rect& finalRect, const gui::Rect& finalClip);
		bool onMouseButton(gui::EventArgs::MouseButtons btn, gui::EventArgs::ButtonState state);
		bool onMouseMove();

		static const char* GetType() { return "Graph"; }
		virtual const char* getType() const { return self::GetType(); }

	protected:
		bool m_pushed;
	};
}