#pragma once

#include <functional>

struct frame_info {
	size_t frame_index;
	double time;
	double prev_time;
	double dt;

};

struct _env{
	frame_info cur_frame;

	std::function<void(int, int)> mouse_move_cb;
	std::function<void(int, int)> mouse_button_cb;
	std::function<void(int, int)> mouse_wheel_cb;
	std::function<void()> update_cb;
	std::function<void()> render_cb;
	std::function<void(int, int)> resize_cb;
};

extern _env env;

struct GLFWwindow;

class BaseApplication {
public:
	BaseApplication(size_t w, size_t h, const char* title);
	~BaseApplication();

	int run();

	const frame_info& getFrameInfo() const;

	int width() const { return m_window_width; }
	int height() const { return m_window_height; }


protected:
	int m_window_width;
	int m_window_height;
	GLFWwindow* window;

	virtual void onWindowSize(int /*w*/,int /*h*/) {}
	virtual int  onWindowclose(void) { return 0;}
	virtual void onWindowrefresh(void) {}
	virtual void onMousebutton(int /*button*/, int /*action*/) {}
	virtual void onMousepos(int /*x*/, int /*y*/) {}
	virtual void onMousewheel(int /*delta*/) {}
	virtual void onKey(int /*key*/, int /*action */) {}
	virtual void onChar( int /*character*/, int /*action */) {}
	
	bool m_ready;

private:
	static void _OnWindowsizefun(GLFWwindow*, int w, int h);
	static void _OnMousebuttonfun(GLFWwindow*, int button, int action, int /*mods*/);
	static void _OnMouseposfun(GLFWwindow*, double x, double y);
	static void _OnKeyfun(GLFWwindow*, int key, int /*scancode*/, int action, int /*mods*/);
};