#include "graph_timeline.h"

namespace gui {

	std::vector<gui::point> points;
	int hovered_point = -1;

Graph::Graph(System& sys, const std::string& name) : FrameWindow(sys, name){
	m_pushed = false;
}

Graph::~Graph() {

}

void Graph::render(const gui::Rect& dest, const gui::Rect& clip) {
	base::render(dest, clip);

	gui::Renderer& r = m_system.getRenderer();

	vec2 center_line[] = {
			{ dest.m_left, dest.getHeight()*0.5f },
			{ dest.m_right, dest.getHeight()*0.5f }
	};

	r.drawLine(*m_backImg, &center_line[0], 2, 4, dest, 0xFFAAAAAA, 1);

	if (points.empty()) return;
	

	static point dp(2.0f, 2.0f);

	r.drawLine(*m_backImg, &points[0], points.size(), 3, dest, 0xCC0000FF, 1);

	
	for (int i = 0; i < points.size(); ++i) {
		auto p = points[i];
		const bool isHovered = i == hovered_point;
		float width = isHovered ? 3 : 2;
		unsigned int color = isHovered ? 0xFF0000FF : 0xFF000000;

		//point line_points[] = {
		//	p - dp,
		//	p - dp*point(-1, 1),
		//	p + dp,
		//	p + dp*point(-1, 1),
		//	p - dp,
		//	p - dp*point(-1, 1)
		//};
		//r.drawLine(*m_backImg, line_points, 6, 4, dest, color, width);

		auto t = p + clip.getPosition();
		r.draw(*m_backImg, Rect(t.x - width, t.y - width, t.x + width, t.y + width), 4, clip, ColorRect(color));
	}
}


bool Graph::onMouseMove() {
	gui::point p = transformToWndCoord(m_system.getCursor().getPosition());
	point newpos = p - m_area.getPosition();

	if (!m_pushed) {
		hovered_point = -1;

		for (int i = 0; i < points.size(); ++i) {
			vec2 dis = newpos - points[i];
			if ((dis.x*dis.x + dis.y*dis.y) < 16) {
				hovered_point = i;
				break;
			}
		}
	}
	else if (hovered_point != -1) {
		points[hovered_point] = newpos;
	}

	return true;
}

bool Graph::onMouseButton(gui::EventArgs::MouseButtons btn, gui::EventArgs::ButtonState state) {
	gui::point pt = transformToWndCoord(m_system.getCursor().getPosition());
	point newpos = pt - m_area.getPosition();

	if (btn == gui::EventArgs::Left) {
		if (state == gui::EventArgs::Down && m_area.isPointInRect(pt)) {
			m_system.queryCaptureInput(this);
			m_pushed = true;
		}
		else if (m_pushed){
			m_system.queryCaptureInput(0);
			m_pushed = false;

			if (hovered_point == -1)
				points.push_back(newpos);

			gui::MouseEventArgs m;
			m.name = "On_Clicked";
			callHandler(&m);
		}
	}
	else if (btn == gui::EventArgs::Right && state == gui::EventArgs::Up && hovered_point != -1) {
		points.erase(points.begin() + hovered_point);
		hovered_point = -1;
	}

	base::onMouseButton(btn, state);
	return true;
}

}