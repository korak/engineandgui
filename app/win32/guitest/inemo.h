#pragma once

#include <vector>
#include <thread>
#include <mutex>
#include <chrono>
#include <algorithm>
#include <atomic>

//#include "inemo/iNEMO_M1_SDK.h"

#include "sensor_filter.h"

#include <glm/gtx/quaternion.hpp>

struct Component_t {
	union {
		struct {double X, Y, Z;};
		struct {double Roll, Pitch, Yaw;};
	};
};

struct FrameData_t {
	Component_t Magnetometer;
	Component_t Gyroscope;
	Component_t Accelerometer;
	Component_t Rotation;
	unsigned int SampleIndex;
	unsigned int TimeStamp;
};

struct InemoData {
	typedef std::vector<FrameData_t> frames_t;
	typedef std::vector<glm::quat> quats_t;
	enum {
		MAX_DATA_FRAMES = 1024,
		MagMaxReferences = 1000
	};

	InemoData(std::string port_name = std::string(),
			  std::string connection_string = std::string());
	~InemoData();

	float pitch = 0;
	float roll = 0;
	float yaw = 0;

	int MagRefScore = 0;

	bool EnableGravity = true;
	bool EnableYawCorrection = true;
	bool EnablePrediction = true;
	bool MagCalibrated = false;
	float DeltaT = 0;
	float Gain = 0.05f;

	glm::vec3 MagRefsInBodyFrame[MagMaxReferences];
	glm::vec3 MagRefsInWorldFrame[MagMaxReferences];

	unsigned int Stage = 0;
	float RunningTime = 0;
	int MagNumReferences = 0;
	int MagRefIdx = -1;

	SensorFilter      FRawMag;
	SensorFilter      FAngV;

	glm::quat          Q;
	glm::quat		   QUncorrected;
	glm::vec3          A;
	glm::vec3          AngV;
	glm::vec3          CalMag;
	glm::vec3          RawMag;

	glm::vec3          GyroOffset;
	SensorFilterBase<float> TiltAngleFilter;

	void ComplementaryFilter(Component_t accData, Component_t gyrData/*, float *pitch, float *roll, float* yaw*/)
	{
		const float ACCELEROMETER_SENSITIVITY = 8000.0f;
		//const float GYROSCOPE_SENSITIVITY = 65.536f;
		const float GYROSCOPE_SENSITIVITY = 50.0f / 2.0f; //250 deg in RADs

		const float M_PI = 3.14159265359f;

		const float dt = 0.01f;							// 10 ms sample rate!    

		float pitchAcc, rollAcc, yawAcc;

		// Integrate the gyroscope data -> int(angularSpeed) = angle
		float dpitch = ((float)gyrData.X / GYROSCOPE_SENSITIVITY) * dt; // Angle around the X-axis
		float droll = ((float)gyrData.Y / GYROSCOPE_SENSITIVITY) * dt;    // Angle around the Y-axis
		float dyaw = ((float)gyrData.Z / GYROSCOPE_SENSITIVITY) * dt; // Angle around the X-axis

		//pitch += ((float)gyrData.X / GYROSCOPE_SENSITIVITY) * dt; // Angle around the X-axis
		//roll -= ((float)gyrData.Y / GYROSCOPE_SENSITIVITY) * dt;    // Angle around the Y-axis
		//yaw += ((float)gyrData.Z / GYROSCOPE_SENSITIVITY) * dt; // Angle around the X-axis


		// Compensate for drift with accelerometer data if !bullshit
		// Sensitivity = -2 to 2 G at 16Bit -> 2G = 32768 && 0.5G = 8192
		int forceMagnitudeApprox = abs(accData.X) + abs(accData.Y) + abs(accData.Z);
		if (forceMagnitudeApprox > 1000 && forceMagnitudeApprox < 1180)
		{
			dpitch = droll = dyaw = 0;
			// Turning around the X axis results in a vector on the Y-axis
			//pitchAcc = atan2f((float)accData.Y, (float)accData.Z);// *180 / M_PI;
			//pitch = pitch * 0.98 + pitchAcc * 0.02;

			//// Turning around the Y axis results in a vector on the X-axis
			//rollAcc = atan2f((float)accData.X, (float)accData.Z);// *180 / M_PI;
			//roll = roll * 0.98 + rollAcc * 0.02;

			//yawAcc = atan2f((float)accData[2], (float)accData[2]) * 180 / M_PI;
			//yaw = yaw * 0.98 + yawAcc * 0.02;
		}

		pitch += dpitch;
		roll -= droll;
		yaw += dyaw;
	}

	void processNewFrame(FrameData_t frame, double dt);

	FrameData_t getFrame(unsigned index) {
		std::lock_guard<std::mutex> lock(m_data_mutex);

		static FrameData_t empty_frame = { 0 };
		if (m_data_frames.empty()) return empty_frame;

		index = std::min<unsigned>(index, m_data_frames.size() - 1);
		return m_data_frames[index];
	}

	glm::quat getRotation(unsigned index) {
		std::lock_guard<std::mutex> lock(m_data_mutex);

		static glm::quat empty_rotations;
		if (m_rotations.empty()) return empty_rotations;

		index = std::min<unsigned>(index, m_rotations.size() - 1);
		return m_rotations[index];
	}

	frames_t getFrames() {
		std::lock_guard<std::mutex> lock(m_data_mutex);
		return m_data_frames;
	}

	quats_t getRotations() {
		std::lock_guard<std::mutex> lock(m_data_mutex);
		return m_rotations;
	}

	unsigned getNumFrames() {
		std::lock_guard<std::mutex> lock(m_data_mutex);

		return m_data_frames.size();
	}

	FrameData_t lastFrame() {
		std::lock_guard<std::mutex> lock(m_data_mutex);

		static FrameData_t empty_frame = { 0 };
		if (m_data_frames.empty()) return empty_frame;
		return m_data_frames.back();
	}

	glm::quat lastRotation() {
		std::lock_guard<std::mutex> lock(m_data_mutex);

		static glm::quat empty_rotations;
		if (m_rotations.empty()) return empty_rotations;
		return m_rotations.back();
	}


	void addFrame(const FrameData_t& frame);
	glm::vec3 GetCalibratedMagValue(const glm::vec3& rawMag) const;
	glm::quat GetPredictedOrientation(float pdt);

	inline operator bool() const { return false;/* hDevice != INEMO_M1_DEVICE_HANDLE_INVALID;*/ }
private:
	//INEMO_M1_DEVICE_HANDLE hDevice = INEMO_M1_DEVICE_HANDLE_INVALID;
	//INEMO_M1_ERROR iErr;

	char szBuffer[256];
	char szPortName[32];

	std::mutex m_data_mutex;
	std::thread m_data_thread;
	frames_t m_data_frames;
	quats_t m_rotations;

	std::atomic<int> m_active_flag;

	std::string m_port_name;
	std::string m_connection_string;
};

