#include "sensor_filter.h"


glm::vec3 SensorFilter::Median() const
{
	int half_window = Count / 2;
	float* sortx = (float*)malloc(Count * sizeof(float));
	float* sorty = (float*)malloc(Count * sizeof(float));
	float* sortz = (float*)malloc(Count * sizeof(float));
	float resultx = 0.0f, resulty = 0.0f, resultz = 0.0f;

	for (int i = 0; i < Count; i++)
	{
		sortx[i] = Elements[i].x;
		sorty[i] = Elements[i].y;
		sortz[i] = Elements[i].z;
	}
	for (int j = 0; j <= half_window; j++)
	{
		int minx = j;
		int miny = j;
		int minz = j;
		for (int k = j + 1; k < Count; k++)
		{
			if (sortx[k] < sortx[minx]) minx = k;
			if (sorty[k] < sorty[miny]) miny = k;
			if (sortz[k] < sortz[minz]) minz = k;
		}
		const float tempx = sortx[j];
		const float tempy = sorty[j];
		const float tempz = sortz[j];
		sortx[j] = sortx[minx];
		sortx[minx] = tempx;

		sorty[j] = sorty[miny];
		sorty[miny] = tempy;

		sortz[j] = sortz[minz];
		sortz[minz] = tempz;
	}
	resultx = sortx[half_window];
	resulty = sorty[half_window];
	resultz = sortz[half_window];

	free(sortx);
	free(sorty);
	free(sortz);

	return glm::vec3(resultx, resulty, resultz);
}

//  Only the diagonal of the covariance matrix.
glm::vec3 SensorFilter::Variance() const
{
	glm::vec3 mean = Mean();
	glm::vec3 total = glm::vec3(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < Count; i++)
	{
		total.x += (Elements[i].x - mean.x) * (Elements[i].x - mean.x);
		total.y += (Elements[i].y - mean.y) * (Elements[i].y - mean.y);
		total.z += (Elements[i].z - mean.z) * (Elements[i].z - mean.z);
	}
	return total / (float)Count;
}

// Should be a 3x3 matrix returned, but OVR_math.h doesn't have one
glm::mat4 SensorFilter::Covariance() const
{
	glm::vec3 mean = Mean();
	glm::mat4 total(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	for (int i = 0; i < Count; i++)
	{
		total.M[0][0] += (Elements[i].x - mean.x) * (Elements[i].x - mean.x);
		total.M[1][0] += (Elements[i].y - mean.y) * (Elements[i].x - mean.x);
		total.M[2][0] += (Elements[i].z - mean.z) * (Elements[i].x - mean.x);
		total.M[1][1] += (Elements[i].y - mean.y) * (Elements[i].y - mean.y);
		total.M[2][1] += (Elements[i].z - mean.z) * (Elements[i].y - mean.y);
		total.M[2][2] += (Elements[i].z - mean.z) * (Elements[i].z - mean.z);
	}
	total.M[0][1] = total.M[1][0];
	total.M[0][2] = total.M[2][0];
	total.M[1][2] = total.M[2][1];
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			total.M[i][j] *= 1.0f / Count;
	return total;
}

glm::vec3 SensorFilter::PearsonCoefficient() const
{
	glm::mat4 cov = Covariance();
	glm::vec3 pearson();
	pearson.x = cov.M[0][1] / (sqrt(cov.M[0][0])*sqrt(cov.M[1][1]));
	pearson.y = cov.M[1][2] / (sqrt(cov.M[1][1])*sqrt(cov.M[2][2]));
	pearson.z = cov.M[2][0] / (sqrt(cov.M[2][2])*sqrt(cov.M[0][0]));

	return pearson;
}