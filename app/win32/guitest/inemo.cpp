#include "inemo.h"

#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>

//#pragma comment(lib, "iNEMO_M1_sdk.lib")

namespace mathf {
	const float Tolerance = 0.00001f;
	const float SingularityRadius = 0.0000001f; // Use for Gimbal lock numerical problems
}


// Compute a rotation required to transform "estimated" into "measured"
// Returns an approximation of the goal rotation in the Simultaneous Orthogonal Rotations Angle representation
// (vector direction is the axis of rotation, norm is the angle)
glm::vec3 SensorFusion_ComputeCorrection(glm::vec3 measured, glm::vec3 estimated)
{
	measured = glm::normalize(measured);
	estimated = glm::normalize(estimated);

	glm::vec3 correction = glm::cross(measured, estimated);
	float cosError = glm::dot(measured, estimated);
	// from the def. of cross product, correction.Length() = sin(error)
	// therefore sin(error) * sqrt(2 / (1 + cos(error))) = 2 * sin(error / 2) ~= error in [-pi, pi]
	// Mathf::Tolerance is used to avoid div by 0 if cos(error) = -1
	return correction * sqrt(2 / (1 + cosError + mathf::Tolerance));
}

// Sensor reports data in the following coordinate system:
// Accelerometer: 10^-4 m/s^2; X forward, Y right, Z Down.
// Gyro:          10^-4 rad/s; X positive roll right, Y positive pitch up; Z positive yaw right.


// We need to convert it to the following RHS coordinate system:
// X right, Y Up, Z Back (out of screen)

glm::vec3 MagFromBodyFrameUpdate(const FrameData_t& update, bool convertHMDToSensor = false)
{
	// Note: Y and Z are swapped in comparison to the Accel.  
	// This accounts for DK1 sensor firmware axis swap, which should be undone in future releases.
	if (!convertHMDToSensor)
	{
		return glm::vec3((float)update.Magnetometer.X,
			(float)update.Magnetometer.Z,
			(float)update.Magnetometer.Y) * 0.001f;
	}

	return glm::vec3((float)update.Magnetometer.X,
		(float)update.Magnetometer.Y,
		-(float)update.Magnetometer.Z) * 0.001f;
}

glm::vec3 EulerFromBodyFrameUpdate(const FrameData_t& update, bool convertHMDToSensor = false)
{
	const FrameData_t& sample = update;// .Samples[sampleNumber];
	float                gx = (float)sample.Gyroscope.X;
	float                gy = (float)sample.Gyroscope.Y;
	float                gz = (float)sample.Gyroscope.Z;

	glm::vec3 val = convertHMDToSensor ? glm::vec3(gx, gz, -gy) : glm::vec3(gx, gy, gz);
	//return val / (1.2f*286.0f);// *0.001f;
	return val * 0.005f;
}

glm::vec3 AccelFromBodyFrameUpdate(const FrameData_t& update, bool convertHMDToSensor = false)
{
	const FrameData_t& sample = update;// .Samples[sampleNumber];
	float ax = (float)sample.Accelerometer.X;
	float ay = (float)sample.Accelerometer.Y;
	float az = (float)sample.Accelerometer.Z;

	glm::vec3 val = convertHMDToSensor ? glm::vec3(ax, az, -ay) : glm::vec3(ax, ay, az);
	return val * 0.0001f;// 0.1f;
}



InemoData::InemoData(std::string port_name, std::string connection_string) {
	m_port_name = port_name.empty() ? "PN=COM4" : port_name;
	m_connection_string = connection_string.empty() ? "DATABUFFERSIZE=1024, TRACEBUFFERSIZE=1024, PL=PL_001{%s, SENDMODE=B}" : connection_string;

	sprintf_s(szPortName, 32, m_port_name.c_str());
	sprintf_s(szBuffer, 256, m_connection_string.c_str(), szPortName);
	//hDevice = INEMO_M1_DEVICE_HANDLE_INVALID;
	//hDevice = INEMO_M1_Connect(szBuffer);

	//if (hDevice != INEMO_M1_DEVICE_HANDLE_INVALID)
	//{
	//	printf("\niNEMO device (%s) Handle opened", szPortName);

	//	//if (INEMO_M1_IsConnect(hDevice))
	//	//{
	//	//	printf("\niNEMO device connected");

	//	//	//iErr = INEMO_M1_Command(hDevice, INEMO_M1_CMD_LED_ON);

	//	//	INEMO_M1_OUTPUT_t outPutConfig;
	//	//	outPutConfig.Mode = INEMO_M1_OUTPUT_MODE_AHRS;      //Set the output mode default
	//	//	outPutConfig.Data = INEMO_M1_OUTPUT_DATA_ALL;       //Get all sensors data 
	//	//	outPutConfig.Frequency = INEMO_M1_OUTPUT_FREQ_400;	//frequency = 50Hz
	//	//	outPutConfig.Samples = 0;                           //number of samples  ----> 0 to run in continous mode    
	//	//	outPutConfig.Type = INEMO_M1_OUTPUT_TYPE_USB;       //set the device output type to usb (Virtual Com)

	//	//	iErr = INEMO_M1_SetOutput(hDevice, &outPutConfig);
	//	//	INEMO_M1_Command(hDevice, INEMO_M1_CMD_START_ACQUISTION);

	//	//	m_active_flag = 1;

	//	//	m_data_thread = std::thread([=](){
	//	//		const long ms = 1000 / outPutConfig.Frequency;
	//	//		const std::chrono::milliseconds dura(ms);

	//	//		static FrameData_t frame = { 0 };

	//	//		while (this->m_active_flag != 0) {
	//	//			memset(&frame, 0, sizeof(frame));
	//	//			INEMO_M1_ERROR iErr = INEMO_M1_GetDataSample(hDevice, &frame);
	//	//			if (iErr == INEMO_M1_ERROR_NONE && frame.ValidFields) {
	//	//				addFrame(frame);
	//	//			}
	//	//			//std::this_thread::sleep_for(dura);
	//	//		}
	//	//	});
	//	//}
	//}
	//else
	{
		printf("\nFailed to connect to iNEMO device (%s)", szPortName);
	}
}

InemoData::~InemoData() {
	m_active_flag.store(0);
	if (m_data_thread.joinable())
		m_data_thread.join();

	//if (hDevice != INEMO_M1_DEVICE_HANDLE_INVALID) {
	//	if (INEMO_M1_IsConnect(hDevice))
	//	{
	//		//send the stop acquisition command
	//		iErr = INEMO_M1_Command(hDevice, INEMO_M1_CMD_STOP_ACQUISTION);
	//		//Switch the led Off
	//		iErr = INEMO_M1_Command(hDevice, INEMO_M1_CMD_LED_OFF);
	//		iErr = INEMO_M1_Disconnect(hDevice);
	//	}
	//	hDevice = INEMO_M1_DEVICE_HANDLE_INVALID;
	//}
}

void InemoData::addFrame(const FrameData_t& frame) {
	std::lock_guard<std::mutex> lock(m_data_mutex);

	if (!m_data_frames.empty()) {
		if (m_data_frames.back().SampleIndex >= frame.SampleIndex ||
			m_data_frames.back().TimeStamp >= frame.TimeStamp)
			return;
	}

	float dt = m_data_frames.empty() ? 0 : (frame.TimeStamp - m_data_frames.back().TimeStamp);

	processNewFrame(frame, dt);

	m_data_frames.push_back(frame);
	m_rotations.push_back(Q);
	//ComplementaryFilter(frame.Accelerometer, frame.Gyroscope);

	if (m_data_frames.size() > MAX_DATA_FRAMES) {
		m_data_frames.erase(m_data_frames.begin());
	}

	if (m_rotations.size() > MAX_DATA_FRAMES) {
		m_rotations.erase(m_rotations.begin());
	}
}

glm::vec3 InemoData::GetCalibratedMagValue(const glm::vec3& rawMag) const
{
	//OVR_ASSERT(HasMagCalibration());
	//return MagCalibrationMatrix.Transform(rawMag);
	return rawMag;
}


//  A predictive filter based on extrapolating the smoothed, current angular velocity
glm::quat InemoData::GetPredictedOrientation(float pdt)
{
	glm::quat qP = Q;

	if (EnablePrediction)
	{
		// This method assumes a constant angular velocity
		glm::vec3 angVelF = FAngV.SavitzkyGolaySmooth8();
		float    angVelFL = glm::length(angVelF);

		// Force back to raw measurement
		angVelF = AngV;
		angVelFL = glm::length(AngV);

		// Dynamic prediction interval: Based on angular velocity to reduce vibration
		const float minPdt = 0.001f;
		const float slopePdt = 0.1f;
		float       newpdt = pdt;
		float       tpdt = minPdt + slopePdt * angVelFL;
		if (tpdt < pdt)
			newpdt = tpdt;
		//LogText("PredictonDTs: %d\n",(int)(newpdt / PredictionTimeIncrement + 0.5f));

		if (angVelFL > 0.001f)
		{
			glm::vec3    rotAxisP = angVelF / angVelFL;
			float       halfRotAngleP = angVelFL * newpdt * 0.5f;
			float       sinaHRAP = sin(halfRotAngleP);
			glm::quat       deltaQP(rotAxisP.x*sinaHRAP, rotAxisP.y*sinaHRAP,
				rotAxisP.z*sinaHRAP, cos(halfRotAngleP));
			qP = Q * deltaQP;
		}
	}
	return qP;
}

namespace glm {
	float angle(const vec3& a, const vec3& b)
	{
		float dist = length2(a)*length2(b);
		float result = acos(dot(a, b)) / sqrtf(dist);// glm::length(b);
		return result;
	}

	glm::vec3 ProjectTo(const glm::vec3& a, const glm::vec3& b)
	{
		float l2 = dot(b,b);
		return b * (dot(a,b) / l2);
	}

	glm::vec3 ProjectToPlane(glm::vec3 v, glm::vec3 normal) {
		return v - ProjectTo(v, normal);
	}
}


void InemoData::processNewFrame(FrameData_t frame, double dt)
{
	dt *= 1000;
	// Put the sensor readings into convenient local variables
	glm::vec3 gyro = EulerFromBodyFrameUpdate(frame, true);// .RotationRate;
	glm::vec3 accel = AccelFromBodyFrameUpdate(frame, true);// .Acceleration;
	glm::vec3 mag = MagFromBodyFrameUpdate(frame, true);// .MagneticField;

	// Insert current sensor data into filter history
	FRawMag.AddElement(mag);
	FAngV.AddElement(gyro);

	// Apply the calibration parameters to raw mag
	glm::vec3 calMag = MagCalibrated ? GetCalibratedMagValue(FRawMag.Mean()) : FRawMag.Mean();

	// Set variables accessible through the class API
	DeltaT = dt;
	AngV = gyro;
	A = accel;
	RawMag = mag;
	CalMag = calMag;

	// Keep track of time
	Stage++;
	RunningTime += DeltaT;

	// Small preprocessing
	glm::quat Qinv = glm::inverse(Q);// .Inverted();
	static const glm::vec3 frame_up(0, 1, 0);
	glm::vec3 up = glm::rotate<float>(Qinv, frame_up);// .Rotate(Vector3f(0, 1, 0));

	glm::vec3 gyroCorrected = gyro;

	// Apply integral term
	// All the corrections are stored in the Simultaneous Orthogonal Rotations Angle representation,
	// which allows to combine and scale them by just addition and multiplication
	if (EnableGravity || EnableYawCorrection)
		gyroCorrected -= GyroOffset;

	if (EnableGravity)
	{
		const float spikeThreshold = 0.01f;
		const float gravityThreshold = 0.1f;
		float proportionalGain = 5 * Gain; // Gain parameter should be removed in a future release
		float integralGain = 0.0125f;

		glm::vec3 tiltCorrection = SensorFusion_ComputeCorrection(accel, up);

		if (Stage > 5)
		{
			// Spike detection
			float tiltAngle = glm::angle(up, accel);

			TiltAngleFilter.AddElement(tiltAngle);
			if (tiltAngle > TiltAngleFilter.Mean() + spikeThreshold)
				proportionalGain = integralGain = 0;
			// Acceleration detection
			const float gravity = 9.8f;
			if (fabs(glm::length(accel) / gravity - 1) > gravityThreshold)
				integralGain = 0;
		}
		else // Apply full correction at the startup
		{
			proportionalGain = 1 / DeltaT;
			integralGain = 0;
		}

		gyroCorrected += (tiltCorrection * proportionalGain);
		GyroOffset -= (tiltCorrection * integralGain * DeltaT);
	}

	if (EnableYawCorrection && MagCalibrated && RunningTime > 2.0f)
	{
		const float maxMagRefDist = 0.1f;
		const float maxTiltError = 0.05f;
		float proportionalGain = 0.01f;
		float integralGain = 0.0005f;
		
		// Update the reference point if needed
		if (MagRefIdx < 0 || glm::distance(calMag, MagRefsInBodyFrame[MagRefIdx]) > maxMagRefDist)
		{
			// Delete a bad point
			if (MagRefIdx >= 0 && MagRefScore < 0)
			{
				MagNumReferences--;
				MagRefsInBodyFrame[MagRefIdx] = MagRefsInBodyFrame[MagNumReferences];
				MagRefsInWorldFrame[MagRefIdx] = MagRefsInWorldFrame[MagNumReferences];
			}
			// Find a new one
			MagRefIdx = -1;
			MagRefScore = 1000;
			float bestDist = maxMagRefDist;
			for (int i = 0; i < MagNumReferences; i++)
			{
				float dist = glm::distance(calMag, MagRefsInBodyFrame[i]);
				if (bestDist > dist)
				{
					bestDist = dist;
					MagRefIdx = i;
				}
			}
			// Create one if needed
			if (MagRefIdx < 0 && MagNumReferences < MagMaxReferences)
			{
				MagRefIdx = MagNumReferences;
				MagRefsInBodyFrame[MagRefIdx] = calMag;
				MagRefsInWorldFrame[MagRefIdx] = glm::normalize(glm::rotate(Q, calMag));
				MagNumReferences++;
			}
		}

		if (MagRefIdx >= 0)
		{
			glm::vec3 magEstimated = glm::rotate(Qinv, MagRefsInWorldFrame[MagRefIdx]);// .Rotate(MagRefsInWorldFrame[MagRefIdx]);
			glm::vec3 magMeasured = glm::normalize(calMag);// .Normalized();

			
			// Correction is computed in the horizontal plane (in the world frame)
			glm::vec3 yawCorrection = SensorFusion_ComputeCorrection(glm::ProjectToPlane(magMeasured, up), glm::ProjectToPlane(magEstimated, up));

			if (fabs(glm::dot(up, (magEstimated - magMeasured) )) < maxTiltError)
			{
				MagRefScore += 2;
			}
			else // If the vertical angle is wrong, decrease the score
			{
				MagRefScore -= 1;
				proportionalGain = integralGain = 0;
			}
			gyroCorrected += (yawCorrection * proportionalGain);
			GyroOffset -= (yawCorrection * integralGain * DeltaT);
		}
	}

	// Update the orientation quaternion based on the corrected angular velocity vector
	float angle = glm::length(gyroCorrected) * DeltaT;
	if (angle > 0.0f)
		Q = Q * glm::angleAxis(angle, gyroCorrected);// glm::quat(gyroCorrected, angle);

	// The quaternion magnitude may slowly drift due to numerical error,
	// so it is periodically normalized.
	if (Stage % 50 == 0)
		Q = glm::normalize(Q);// .Normalize();
}