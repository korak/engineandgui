#include "base_application.h"
#include <GLFW/glfw3.h>
#include <windows.h>

#include <algorithm>

_env env = { 0 };

BaseApplication::BaseApplication(size_t w, size_t h, const char* title) 
: m_ready(false), window(nullptr) {

	m_window_width = (int)w;
	m_window_height = (int)h;

	if (glfwInit() != GL_TRUE) return; 
		
	{
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);


		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
		glfwWindowHint(GLFW_RED_BITS, 8);
		glfwWindowHint(GLFW_GREEN_BITS, 8);		
		glfwWindowHint(GLFW_BLUE_BITS, 8);
		glfwWindowHint(GLFW_ALPHA_BITS, 8);
		glfwWindowHint(GLFW_DEPTH_BITS, 24);
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		glfwWindowHint(GLFW_SAMPLES, 8);

		glfwSwapInterval(1);
		//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

		window = glfwCreateWindow(w, h, title, NULL, NULL);

		

		if (!glfwInit())
			exit(EXIT_FAILURE);

		glfwSetWindowUserPointer(window, this);

		if (!window)
		{
			glfwTerminate();
			exit(EXIT_FAILURE);
		}

		glfwMakeContextCurrent(window);

		// Window resize callback function
		glfwSetWindowSizeCallback(window, _OnWindowsizefun );
		//// Set keyboard input callback function
		glfwSetKeyCallback(window, _OnKeyfun);
		glfwSetMouseButtonCallback(window, _OnMousebuttonfun);
		glfwSetCursorPosCallback(window, _OnMouseposfun);

		int framebuff_w, framebuff_h;
		glfwGetFramebufferSize(window, &framebuff_w, &framebuff_h);

		int window_w, window_h;
		glfwGetWindowSize(window, &window_w, &window_h);


		m_ready = true;
	}
}

BaseApplication::~BaseApplication() {
	glfwSetWindowUserPointer(window, nullptr);
	glfwDestroyWindow(window);
	glfwTerminate();
}


const frame_info& BaseApplication::getFrameInfo() const {
	return env.cur_frame;
}

int BaseApplication::run() {
	if (!m_ready) return -1;

	// the time of the previous frame
	env.cur_frame.prev_time = env.cur_frame.time = glfwGetTime();
	env.cur_frame.dt = 0.00001;
	env.cur_frame.frame_index = 0;
	// this just loops as long as the program runs
	glfwMakeContextCurrent(window);
	GLenum err = glGetError();


	while (!glfwWindowShouldClose(window))
	{

		glfwGetWindowSize(window, &m_window_width, &m_window_height);

		// calculate time elapsed, and the amount by which stuff rotates
		env.cur_frame.prev_time = env.cur_frame.time;
		env.cur_frame.time = glfwGetTime();
		env.cur_frame.dt = env.cur_frame.time - env.cur_frame.prev_time;

		env.cur_frame.dt = std::max<double>(env.cur_frame.dt, 0.000001);
		

		// escape to quit, arrow keys to rotate view
		//if (glfwGetKey(GLFW_KEY_ESC) == GLFW_PRESS)
		//	break;
		GLenum err = glGetError();

		glDisable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);

		glViewport(0, 0, m_window_width, m_window_height);

		glClearColor(0.32f, 0.33f, 0.35f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glDisable(GL_DEPTH_TEST);

		if (env.update_cb)
			env.update_cb();

		if (env.render_cb)
			env.render_cb();

		glfwSwapBuffers(window);
		glfwPollEvents();
		//Sleep(50);
		env.cur_frame.frame_index++;
	}

	return 0;
}

void BaseApplication::_OnWindowsizefun(GLFWwindow* window, int w, int h) {
	BaseApplication* app = (BaseApplication*)glfwGetWindowUserPointer(window);
	app->onWindowSize(w, h);
}

void BaseApplication::_OnMousebuttonfun(GLFWwindow* w, int button, int action, int /*mods*/) {
	BaseApplication* app = (BaseApplication*)glfwGetWindowUserPointer(w);
	app->onMousebutton(button, action);
}

void BaseApplication::_OnMouseposfun(GLFWwindow* w, double x, double y) {
	BaseApplication* app = (BaseApplication*)glfwGetWindowUserPointer(w);
	app->onMousepos(x, y);
}

void BaseApplication::_OnKeyfun(GLFWwindow* w, int key, int /*scancode*/, int action, int /*mods*/) {
	BaseApplication* app = (BaseApplication*)glfwGetWindowUserPointer(w);
	app->onKey(key, action);
}