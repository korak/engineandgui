#include <windows.h>

#include "guiapplication.h"

#undef ERROR
#include <glm/glm.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/spline.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <engine/engine.h>
#include <engine/LineDrawer.h>
#include <engine/debug_shapes.h>

#include "props.h"

//#include "inemo/iNEMO_M1_SDK.h"
#include "inemo.h"

float toRad(float a) { return a*3.1415f / 180.0f; }

class SensorsApp : public GuiApplication {
	size_t frame_id = 0;
	InemoData inemo;
public:
	SensorsApp() : GuiApplication(1024, 768, "Sensors Test")
	{
		rotate_y = 0;
		rotate_z = 0;

		glm::vec4 cam_pos(-10, 10, 0, 1);
		m_camera.projection(45.0f, (float)m_window_width / (float)m_window_height, 0.1f, 2000.0f);
		m_camera.lookat(cam_pos, glm::vec4(0, 0, 0, 1), glm::vec4(0, 1, 0, 1));


		auto model = Model::CreateTeapot();
		model->position(glm::vec3(0, 0, 0));
		models.push_back(model);

		//for (frame_id = 0; frame_id < 1024; ++frame_id) {
		//	DataFrame frame = { 
		//		frame_id*0.01f, 
		//		sinf(frame_id*0.01f),
		//		sinf(1+frame_id*0.01f),
		//		cosf(frame_id*0.01f)
		//	};
		//	frames.push_back(frame);
		//}

		m_axis_canvas.axis3d();
	}

	~SensorsApp() {

	}

	virtual void update() {
		GuiApplication::update();
	}

	void drawFrames(const InemoData::frames_t& frames) {
		//auto frames = inemo.getFrames();

		if (frames.size() < 2) return;

		m_font->drawText("iNemo:", gui::Rect(5, 5, 200, 200), 1.f);

		auto offset = frames.front().TimeStamp;

		float x_scale = 400;
		float y_offset = 70;
		float y_offset_back = 0;
		float y_scale = 20;

		glm::vec3 backgound_plane[] = {
				{ 0, y_offset_back, 10 },
				{ 1024, y_offset_back, 10 },
				{ 0, y_offset_back + 2 * y_offset, 10 },
				{ 1024, y_offset_back + 2 * y_offset, 10 }
		};

		m_canvas2d.add(backgound_plane[0], backgound_plane[1], backgound_plane[3], 0x50FFFFFF);
		m_canvas2d.add(backgound_plane[3], backgound_plane[2], backgound_plane[0], 0x50FFFFFF);

		m_canvas2d.add({ 0, y_offset_back, 10 }, { 1024, y_offset_back, 10 }, 0xFFFFFFFF);
		m_canvas2d.add({ 0, y_offset_back + y_offset * 2, 10 }, { 1024, y_offset_back + 2 * y_offset, 10 }, 0xFFFFFFFF);

		for (size_t i = 1; i < frames.size(); ++i) {
			const FrameData_t& prev = frames[i - 1];
			const FrameData_t& cur = frames[i];

			glm::vec3 v[] = {
					{ (prev.TimeStamp - offset) * x_scale, y_offset + toRad(prev.Rotation.Roll) * y_scale, 10 },
					{ (cur.TimeStamp - offset) * x_scale, y_offset + toRad(cur.Rotation.Roll) * y_scale, 10 },
					{ (prev.TimeStamp - offset) * x_scale, y_offset + toRad(prev.Rotation.Pitch) * y_scale, 10 },
					{ (cur.TimeStamp - offset) * x_scale, y_offset + toRad(cur.Rotation.Pitch) * y_scale, 10 },
					{ (prev.TimeStamp - offset) * x_scale, y_offset + toRad(prev.Rotation.Yaw) * y_scale, 10 },
					{ (cur.TimeStamp - offset) * x_scale, y_offset + toRad(cur.Rotation.Yaw) * y_scale, 10 }
			};

			m_canvas2d.add(v[0], v[1], 0xFF0000FF);
			m_canvas2d.add(v[2], v[3], 0xFF00FF00);
			m_canvas2d.add(v[4], v[5], 0xFFFF0000);
		}


		m_canvas2d.add({ 0, y_offset_back, 10 }, { 1024, y_offset_back, 10 }, 0xFFFFFFFF);
		m_canvas2d.add({ 0, y_offset * 2, 10 }, { 1024, y_offset*2, 10 }, 0xFFFFFFFF);
		m_canvas2d.add({ 0, y_offset, 10 }, { 1024, y_offset, 10 }, 0xFFFFFFFF);
	}

	void drawQFrames(const InemoData::frames_t& frames) {
		//auto frames = inemo.getFrames();
		auto qframes = inemo.getRotations();

		if (frames.size() < 2) return;

		m_font->drawText("Filtered:", gui::Rect(5, 160, 200, 200), 1.f);

		auto offset = frames.front().TimeStamp;

		float x_scale = 400;
		float y_offset = 70;
		float y_offset_back = 150;
		float y_scale = 10;

		glm::vec3 backgound_plane[] = {
				{ 0, y_offset_back, 10 },
				{ 1024, y_offset_back, 10 },
				{ 0, y_offset_back + 2 * y_offset, 10 },
				{ 1024, y_offset_back + 2 * y_offset, 10 }
		};

		m_canvas2d.add(backgound_plane[0], backgound_plane[1], backgound_plane[3], 0x50FFFFFF);
		m_canvas2d.add(backgound_plane[3], backgound_plane[2], backgound_plane[0], 0x50FFFFFF);

		m_canvas2d.add({ 0, y_offset_back, 10 }, { 1024, y_offset_back, 10 }, 0xFFFFFFFF);
		m_canvas2d.add({ 0, y_offset_back*1.5f, 10 }, { 1024, y_offset_back*1.5f, 10 }, 0xFFFFFFFF);
		m_canvas2d.add({ 0, y_offset_back + y_offset * 2, 10 }, { 1024, y_offset_back + 2 * y_offset, 10 }, 0xFFFFFFFF);

		

		for (size_t i = 1; i < frames.size(); ++i) {
			const FrameData_t& prev = frames[i - 1];
			const FrameData_t& cur = frames[i];

			glm::quat prevq = glm::normalize(qframes[i - 1]);
			glm::quat curq = glm::normalize(qframes[i]);

			glm::vec3 prev_rot = glm::eulerAngles(prevq);
			glm::vec3 cur_rot = glm::eulerAngles(curq);

			glm::vec3 v[] = {
					{ (prev.TimeStamp - offset) * x_scale, y_offset_back*1.5f + toRad(prev_rot[0]) * y_scale, 10 },
					{ (cur.TimeStamp - offset) * x_scale, y_offset_back*1.5f + toRad(cur_rot[0]) * y_scale, 10 },
					{ (prev.TimeStamp - offset) * x_scale, y_offset_back*1.5f + toRad(prev_rot[1]) * y_scale, 10 },
					{ (cur.TimeStamp - offset) * x_scale, y_offset_back*1.5f + toRad(cur_rot[1]) * y_scale, 10 },
					{ (prev.TimeStamp - offset) * x_scale, y_offset_back*1.5f + toRad(prev_rot[2]) * y_scale, 10 },
					{ (cur.TimeStamp - offset) * x_scale, y_offset_back*1.5f + toRad(cur_rot[2]) * y_scale, 10 }
					//{ (prev.TimeStamp - offset) * x_scale, 210 + toRad(glm::roll(prevq)) * y_scale, 10 },
					//{ (cur.TimeStamp - offset) * x_scale, 210 + toRad(glm::roll(curq)) * y_scale, 10 },
					//{ (prev.TimeStamp - offset) * x_scale, 210 + toRad(glm::pitch(prevq)) * y_scale, 10 },
					//{ (cur.TimeStamp - offset) * x_scale, 210 + toRad(glm::pitch(curq)) * y_scale, 10 },
					//{ (prev.TimeStamp - offset) * x_scale, 210 + toRad(glm::yaw(prevq)) * y_scale, 10 },
					//{ (cur.TimeStamp - offset) * x_scale, 210 + toRad(glm::yaw(curq)) * y_scale, 10 }
			};

			m_canvas2d.add(v[0], v[1], 0xFF0000FF);
			m_canvas2d.add(v[2], v[3], 0xFF00FF00);
			m_canvas2d.add(v[4], v[5], 0xFFFF0000);
		}


		m_canvas2d.add({ 0, y_offset_back, 10 }, { 1024, y_offset_back, 10 }, 0xFFFFFFFF);
		m_canvas2d.add({ 0, y_offset * 2, 10 }, { 1024, y_offset * 2, 10 }, 0xFFFFFFFF);
		m_canvas2d.add({ 0, y_offset, 10 }, { 1024, y_offset, 10 }, 0xFFFFFFFF);
	}

	virtual void render() {
		

		auto frames = inemo.getFrames();

		drawFrames(frames);
		drawQFrames(frames);

		GuiApplication::render();


		//////////////////////////////////////////////////////////////////////////
		{
			auto frames = inemo.getFrames();
			if (!frames.empty()) {
				auto last_frame = frames.back();

				auto model = models.back();

				//glm::quat q({ toRad(last_frame.Rotation.Pitch), toRad(last_frame.Rotation.Yaw), toRad(last_frame.Rotation.Roll) });

				//glm::quat q({ inemo.roll, inemo.pitch, inemo.yaw});
				//glm::quat q({ inemo.pitch, inemo.yaw, inemo.roll });
				//model->rotation(q);

				//glm::quat q(glm::vec3(frame.value * 3.1415, frame.value1 * 3.1415, frame.value2 * 3.1415));
				model->rotation(glm::normalize(inemo.Q));

				static short gyro_max = 0;
				gyro_max = std::max<short>(gyro_max, abs(last_frame.Gyroscope.X));
				gyro_max = std::max<short>(gyro_max, abs(last_frame.Gyroscope.Y));
				gyro_max = std::max<short>(gyro_max, abs(last_frame.Gyroscope.Z));

				using namespace std;
				cout << gyro_max << endl;
			}
		}
		//////////////////////////////////////////////////////////////////////////



		//{
		//	if (frames.size() > 1024)
		//		frames.erase(frames.begin());
		//	frame_id++;
		//	//DataFrame frame = {
		//	//	frame_id*0.01f,
		//	//	sinf(frame_id*0.01f),
		//	//	sinf(1 + frame_id*0.01f),
		//	//	cosf(frame_id*0.01f)
		//	//};
		//	//frames.push_back(frame);
		//}
		
		m_canvas3d.grid3d();
		m_canvas3d.render(m_camera.view_matrix(), m_camera.proj_matrix());

		for (auto model : models) {
			//const DataFrame& frame = frames.back();
			//glm::quat q(glm::vec3(frame.value * 3.1415, frame.value1 * 3.1415, frame.value2 * 3.1415));
			//model->rotation(q);
			model->render(m_camera.view_matrix(), m_camera.proj_matrix());
			
			m_axis_canvas.render(m_camera.view_matrix()*model->local_tm(), m_camera.proj_matrix());
		}
		
		m_canvas2d.render2d(0, 0, width(), height());

		m_canvas2d.clean();
		m_canvas3d.clean();

	}

protected:
	utility::Canvas m_canvas2d;
	utility::Canvas m_canvas3d;

	utility::Canvas m_axis_canvas;

	float rotate_y, rotate_z;

	struct DataFrame {
		float time;
		float value;
		float value1;
		float value2;
	};

	//std::vector<DataFrame> frames;

	Camera m_camera;

	std::vector<std::shared_ptr<Model>> models;
	debug_shapes shapes;
};

class A {
public:
	static void reg(){
		registry::reg("value", &A::value);
		registry::reg("bvalue", &A::get, &A::set);
		registry::reg("cvalue", &A::cget);
		registry::reg("cvalue", &A::dvalue, true);
		registry::reg("const_value", &A::const_value);
	}
	A() :value(-1), const_value(10.0f){
	}
private:
	void set(int i) {
		bvalue = i;
	}
	int get() const {
		return bvalue;
	}
	int cget() const { return cvalue; }
	int value, bvalue, cvalue, dvalue;
	const float const_value;
};


void test_props()
{
	A::reg();
	const property& p = registry::get<A>("value");
	if (!p) return;
	A a;
	p.set(&a, 2);
	p.set(&a, "5");
	std::string svalue = p.get<A, std::string>(&a);
	int ivalue = p.get<A, int>(&a);
	const property& pc = registry::get<A>("const_value");
	if (!pc) return;
	float const_value = pc.get<A, float>(&a);
	const property& pb = registry::get<A>("bvalue");
	pb.set(&a, 5);
	int bv = pb.get<A, int>(&a);

	registry::purge();
}

//INT WINAPI wWinMain( HINSTANCE, HINSTANCE, LPWSTR params, int )
int main()
{
	
    // Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
    _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	//_CrtSetBreakAlloc(9799);
#endif
	
	//test_props();

	{
		SensorsApp app;
		app.run();
	}

    return 0;
}