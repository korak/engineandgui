#pragma once

#include <guilib/guilib.h>

namespace utils {
	typedef std::vector<float> data_t;
	float max(const data_t& data);
	float min(const data_t& data);
	void scale(data_t& data, size_t start, size_t end, float coeff);
	void set(data_t& data, size_t start, size_t end, float coeff);
	void normalize(data_t& data, size_t start, size_t end);
	void add(data_t& data, size_t start, size_t end, float v);
}

class Graph : public gui::Panel {
public:
	typedef std::vector<float> data_t;
	typedef Graph Self;
	typedef gui::Panel base;

	Graph(gui::System& sys, const std::string& name = std::string());
	virtual ~Graph();

	static const char* GetType() { return "Graph"; }
	virtual const char* getType() { return Self::GetType(); }

	virtual void render(const gui::Rect& finalRect, const gui::Rect& finalClip);

	data_t& data() {return m_data;}

	size_t get_view_start() const {return m_view_start;}
	size_t get_view_end() const {return m_view_end;}

	void set_view_start(size_t start) {m_view_start = start;}
	void set_view_end(size_t end) {m_view_end = end;}
	
	void selection_start(size_t start) {m_selection_start = start;}
	void selection_end(size_t end) {m_selection_end = end;}

	void set_cursor_pos(size_t pos) {m_cursor_pos = pos;}
	size_t get_cursor_pos() const {return m_cursor_pos;}

	size_t selection_start() const {return m_selection_start;}
	size_t selection_end() const {return m_selection_end;}

	inline bool has_selection() const {return m_selection_start < m_selection_start;}

	void reset_view();

protected:
	void renderFrame(const gui::Rect& dest, const gui::Rect& clip);
	void draw_selection(size_t start, size_t end);

	virtual bool onMouseMove();
	virtual bool onMouseButton(gui::EventArgs::MouseButtons btn, gui::EventArgs::ButtonState state);

protected:	
	data_t m_data;

	size_t m_selection_start;
	size_t m_selection_end;

	size_t m_view_start;
	size_t m_view_end;

	size_t m_cursor_pos;

	gui::point m_selection_point_start;
	gui::point m_selection_point_end;

	gui::TexturePtr m_line_texure;
	gui::ImagesetPtr m_imgset;

	bool m_pushed;
};