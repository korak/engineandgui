
#include "graph.h"
#include <algorithm>

namespace utils {
	float max(const data_t& data) {
		return *std::min_element(data.begin(), data.end());
	}

	float min(const data_t& data) {
		return *std::max_element(data.begin(), data.end());
	}

	float max(const data_t& data, size_t start, size_t end) {
		return *std::min_element(data.begin()+start, data.begin()+end);
	}

	float min(const data_t& data, size_t start, size_t end) {
		return *std::max_element(data.begin()+start, data.begin()+end);
	}

	void scale(data_t& data, size_t start, size_t end, float coeff) {
		if (start >= end || start >= data.size()) return;

		for (size_t i = start; i < end; ++i)
			data[i] = data[i] * coeff;
	}

	void set(data_t& data, size_t start, size_t end, float coeff) {
		if (start >= end || start >= data.size()) return;

		for (size_t i = start; i < end; ++i)
			data[i] = coeff;
	}

	void normalize(data_t& data, size_t start, size_t end) {
		if (start >= end || start >= data.size()) return;

		float gmin = ::utils::min(data);
		float gmax = ::utils::max(data);

		float gscale = gmax - gmin;

		float min = ::utils::min(data, start, end);
		float max = ::utils::max(data, start, end);

		float scale = max - min;

		for (size_t i = start; i < end; ++i)
			data[i] = gmin + gscale*(data[i] - min)/scale;
	}

	void add(data_t& data, size_t start, size_t end, float v) {
		if (start >= end || start >= data.size()) return;
		for (size_t i = start; i < end; ++i)
			data[i] = data[i] + v;
	}
}

Graph::Graph(gui::System& sys, const std::string& name)
: base(sys, name)
, m_selection_end(0)
, m_selection_start(0)
, m_view_end(0)
, m_view_start(0)
, m_pushed(false)
, m_cursor_pos(0)
{
	this->setVisible(true);
	m_line_texure = m_system.getRenderer().createTexture("bar_white.png");
	m_imgset.reset( new gui::Imageset("Graph"));

	size_t ordinal = m_imgset->AppendTexture(m_line_texure);

	gui::Rect rc(0, 0, m_line_texure->getWidth(), m_line_texure->getHeight());

	gui::SubImage info;
	info.m_ordinal = ordinal;
	info.m_src = rc;
	gui::Image::SubImages data;
	data.push_back(info);

	m_imgset->DefineImage("default", rc.getSize(), data);
}

Graph::~Graph() {

}

void Graph::render(const gui::Rect& finalRect, const gui::Rect& finalClip) {
	base::render(finalRect, finalClip);
	renderFrame(finalRect, finalClip);
}

void Graph::reset_view() {
	m_selection_end = m_selection_start = 0;
	m_view_start = 0;
	m_view_end = m_data.size();
}

void Graph::renderFrame(const gui::Rect& dest, const gui::Rect& clip) {
	gui::Renderer& r = m_system.getRenderer();

	const std::vector<float>& w = m_data;

	if (w.empty()) {
		return;
	}

	if (m_view_start == m_view_end || m_view_end == 0) {
		m_view_start = 0;
		m_view_end = w.size();
	}

	const float delta = (dest.getWidth()) / (m_view_end - m_view_start);

	float min = utils::min(w);
	float max = utils::max(w);

	static std::vector<gui::vec2> p;
	// as we render to dynamic window we need to refresh each frame
	//if (p.size() != v1.size()) 
	{
		p.resize(m_view_end - m_view_start);

		for(size_t i = m_view_start; i < m_view_end; ++i) {
			gui::vec2& _v = p[i-m_view_start];
			_v.x = dest.m_left + (i-m_view_start)*delta;
			_v.y = w[i];
		}

		float scale = dest.getHeight()/(max - min);

		for(size_t i = m_view_start; i < m_view_end; ++i) {
			gui::vec2& v = p[i-m_view_start];
			v.y = (dest.m_top) + ((v.y-min)  * scale);
		}
	}

	const float half_h = dest.m_bottom-dest.getHeight()*0.5f;

	//r.draw()
	unsigned int sel_start = dest.m_left + (m_selection_start - m_view_start)*delta;
	unsigned int sel_width = (m_selection_end - m_selection_start)*delta;	
	unsigned int cursor_pos = dest.m_left + (m_cursor_pos-m_view_start)*delta;
	//r.draw(*state.rightImg, componentRect, 1.f, finalClip,  m_backColor, Image::Stretch, Image::Stretch);

	if (m_imgset && !p.empty()) {
		if (const gui::Image* img = m_imgset->GetImage("default")) {

			if (m_pushed) {
				// we are in selection drawing mode
				gui::point p1 = m_selection_point_start;
				gui::point p2 = m_selection_point_end;

				if (p1.x > p2.x) std::swap(p1, p2);

				gui::Rect rc(p1.x, dest.m_top, p2.x, dest.m_bottom);
				r.draw(*img, rc, 1, dest, gui::ColorRect(0x5000BB00), 
					gui::ImageOps::Stretch, gui::ImageOps::Stretch);
			}
			// we have any selection
			else if (m_selection_start != m_selection_end) {
				r.draw(*img, gui::Rect(sel_start, dest.m_top, sel_start + sel_width, dest.m_bottom), 1, 
					dest, gui::ColorRect(0x5000BB00), gui::ImageOps::Stretch, gui::ImageOps::Stretch);
			}

			struct v2 {float x, y;};

			v2 base_line[] = {{dest.m_left,half_h}, {dest.m_right,half_h}};
			r.drawLine(*img, (gui::vec2*)base_line, 2, 1, dest, 0xFF00FF00, 1);

			r.drawLine(*img, (gui::vec2*)&p[0], p.size(), 1, dest, 0xFFFF0F0F, 1);

			v2 cursor_line[] = {{cursor_pos,dest.m_top}, {cursor_pos,dest.m_bottom}};
			r.drawLine(*img, (gui::vec2*)cursor_line, 2, 1, dest, 0xFF0000FF, 1);
		}
	}
}

bool Graph::onMouseMove() {
	gui::point p = transformToWndCoord(m_system.getCursor().getPosition());

	if (m_pushed) {
		m_selection_point_end = p;
	}

	return true;
}

void Graph::draw_selection(size_t start, size_t end) {

}

bool Graph::onMouseButton(gui::EventArgs::MouseButtons btn, gui::EventArgs::ButtonState state) {
	if(state == gui::EventArgs::Down && btn == gui::EventArgs::Left) {
		gui::point pt = transformToWndCoord(m_system.getCursor().getPosition());
		if(m_area.isPointInRect(pt)) {
			m_selection_point_start = pt;
			m_selection_point_end = pt;
			m_system.queryCaptureInput(this);
			m_pushed = true;
			m_selection_start = 0;
			m_selection_end = 0;
			//invalidate();
		}
	}
	else if (m_pushed){
		m_system.queryCaptureInput(0);
		m_pushed = false;

		gui::point pt = transformToWndCoord(m_system.getCursor().getPosition());

//		if(m_area.isPointInRect(pt))
		{			
			float x1 = m_selection_point_start.x - getArea().m_left;
			float x2 = m_selection_point_end.x - getArea().m_left;

			if (x2 < x1) std::swap(x2, x1);

			if (x2 - x1 < 2) {
				m_selection_start = 0;
				m_selection_end = 0;
			}
			else {
				size_t w = m_view_end - m_view_start;

				float delta = w / m_area.getWidth();
				m_selection_start = floor(x1 * delta + m_view_start);
				m_selection_end = floor(x2 * delta + m_view_start);
				int i = 5;
			}

			gui::MouseEventArgs m;
			m.name = "On_Clicked";
			callHandler(&m);

			gui::events::ClickEvent e;
			send_event(e);
		}
		//else {
		//	m_selection_start = 0;
		//	m_selection_end = 0;
		//}
	}

	base::onMouseButton(btn, state);
	return true;
}