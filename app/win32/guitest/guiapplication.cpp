
#include "guiapplication.h"
#include <guiplatform/renderer_ogl.h>

#include <iostream>
#include <functional>
#include "gui_log.h"

gui_log g_log;

#include "../common/gui_filesystem.h"
#include "graph_timeline.h"
#include <guilib/src/factory.h>

GuiApplication::GuiApplication(int w, int h, const char* title)
	: BaseApplication(w, h, title)
	, m_render(NULL)
	, m_system(NULL)
	, m_elapsed(0)
	, m_active(true)
	, m_needReload(false)
{
	using namespace std::placeholders;
	env.render_cb = std::bind(&GuiApplication::render, this);
	env.update_cb = std::bind(&GuiApplication::update, this);

	m_filename = "guitest\\test.xml";

	update();	
}

GuiApplication::~GuiApplication()
{
	m_fileWatcher.removeWatch(m_watchID);
	m_system.reset();
	m_render.reset();
}

void GuiApplication::run()
{	
	createGUISystem();
	BaseApplication::run();
}

void GuiApplication::createGUISystem()
{
	gui_filesystem* fsp = new gui_filesystem("/data/");
	filesystem_ptr fs(fsp);

	m_watchID = m_fileWatcher.addWatch(fsp->root(), this, true);

	m_render_device = std::make_shared<gui::ogl_platform::RenderDeviceGL>(fs, 1024);
	m_render = std::make_shared<gui::Renderer>(*m_render_device, fs);

	if (m_system)
		m_system.reset();

	m_system = std::make_shared<System>(*m_render, "default", nullptr, g_log);

	m_system->getWindowManager().getFactory().RegisterCreator<gui::Graph>();


	if(m_system)
	{
		Cursor& cursor = m_system->getCursor();
		cursor.setType("CursorNormal");
		m_font = m_system->getWindowManager().loadFont("OSDDefault");
	}

	m_fileWatcher.watch();

	using namespace luabind;
	module(m_system->getScriptSystem().getLuaState())
		[
			class_ <gui::Graph, bases<gui::FrameWindow> >("Graph")
		];

	load(m_filename);
}

void GuiApplication::resetGUISystem()
{
	if(m_render)
		m_render->clearRenderList();

	if(m_system)
		m_system->reset();

	using namespace luabind;
	module(m_system->getScriptSystem().getLuaState())
		[
			class_ <gui::Graph, bases<gui::FrameWindow> >("Graph")
		];

	load(m_filename);
}

void GuiApplication::update()
{
	if (m_needReload) {
		m_needReload = false;
		resetGUISystem();
		m_framecount = 0;
	}

	m_framecount++;
	if(m_system)
	{
		m_system->tick(env.cur_frame.dt);
		m_system->draw();
	}	
}

void GuiApplication::render()
{
	if (m_system)
	{		
		gui::Renderer& r = m_system->getRenderer();
		struct vec2 {float x, y;};
		vec2 points[] = 
		{
			{0,50}, 
			{70,50}, 
			{80,90},
			{110,0},
			{130,60},
			{150,50},
			{260,50},
		};

		gui::Imageset* imageset = m_system->getWindowManager().getImageset("skin");
		const gui::Image* img = imageset->GetImage("Background");

		//r.drawLine(*img, (gui::vec2*)points, 7, 1, gui::Rect(0,0,400,400), 0xFFFF0F0F, 2);

		//m_font->drawText("THE XXXX", gui::Rect(20, 20, 200, 200), 1.f);

		m_system->render();
	}
}

bool GuiApplication::isFinished() 
{
	return false;
}

void GuiApplication::handleViewportChange()
{
	if(!m_system) return;

	gui::event e = {0};
	e.type = gui::event_viewport_resize;
	m_system->handle_event(e);
}


bool GuiApplication::handleMouseButton(EventArgs::MouseButtons btn, EventArgs::ButtonState state)
{
	if(m_system) {
		gui::event e = {0};
		e.type = gui::event_mouse | gui::mouse_button;
		e.type |= (state == EventArgs::Down) ? gui::event_key_down : gui::event_key_up;

		switch(btn) {
			case EventArgs::Left:
				e.mouse.button = gui::button_left;
				break;
			case EventArgs::Middle:
				e.mouse.button = gui::button_middle;
				break;
			case EventArgs::Right:
				e.mouse.button = gui::button_right;
				break;
		}

		return m_system->handle_event(e);
	}
	else
		return false;
}

void GuiApplication::onMousebutton(int button, int action) {
	if (!m_system || button > 2 || action > 1) return;
	int gui_buttons_map[] = { gui::button_left, gui::button_right, gui::button_middle };
	gui::event_type gui_actions_map[] = { gui::event_key_up, gui::event_key_down };

	gui::event e = { 0 };
	e.type = gui::event_mouse | gui::mouse_button;
	e.type |= gui_actions_map[action];
	e.mouse.button = gui_buttons_map[button];
	m_system->handle_event(e);
}

void GuiApplication::onMousepos(int x, int y) {
	mouse_x = x;
	mouse_y = y;
	if (!m_system) return;

	gui::event e = {0};
	e.type = gui::event_mouse | gui::mouse_move;
	e.mouse.x = mouse_x;
	e.mouse.y = mouse_y;
	m_system->handle_event(e);
}

void GuiApplication::onMousewheel(int delta) {
	if (!m_system) return;
	gui::event e = {0};
	e.type = gui::event_mouse | gui::mouse_wheel;
	e.mouse.delta = delta;
	m_system->handle_event(e);
}

void GuiApplication::handleFileAction(efsw::WatchID /*watchid*/, const std::string& /*dir*/, const std::string& filename, efsw::Action action, std::string oldFilename)
{
	if (efsw::Actions::Modified == action) {
		m_needReload = true;
	}
}

void GuiApplication::onKey(int key, int action) {
	if (!m_filename.empty() && key == 294 && action == 1)
	{
		resetGUISystem();
		return;
	}

	if (!m_system) return;
}

void GuiApplication::onChar(int /*character*/, int /*action*/) {
	if (!m_system) return;
}

void GuiApplication::load(const std::string& xml)
{
	if(!m_system) return;
	/*gui::base_window* wnd =*/ m_system->loadXml(xml);
	//auto wnd = std::make_shared<gui::Graph>(*m_system, "Test Graph");
	//m_system->getRootWindow().add(wnd);
}
