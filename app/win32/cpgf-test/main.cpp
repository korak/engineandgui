
#include "props.h"

#include "cpgf/gtestutil.h"

#include "cpgf/gmetadefine.h"
#include "cpgf/goutmain.h"

#include "cpgf/gmetaapiutil.h"
#include "cpgf/gscopedinterface.h"

#include "cpgf/gcallback.h"
#include "cpgf/gcallbacklist.h"

//const constexpr auto add_one = [](auto x)
//{
//	return x + 1;
//};

class A {
public:
	static void reg(){
		registry::reg("value", &A::value);
		registry::reg("bvalue", &A::get, &A::set);
		registry::reg("cvalue", &A::cget);
		registry::reg("cvalue", &A::dvalue, true);
		registry::reg("const_value", &A::const_value);
	}
	A() :value(-1), const_value(10.0f){
	}

	A& operator=(A&&) = delete;
private:
	void set(int i) {
		bvalue = i;
	}
	int get() const {
		return bvalue;
	}
	int cget() const { return cvalue; }
	int value, bvalue, cvalue, dvalue;
	const float const_value;
};


void test_props()
{
	A::reg();
	const property& p = registry::get<A>("value");
	if (!p) return;
	A a;
	p.set(&a, 2);
	p.set(&a, "5");
	std::string svalue = p.get<A, std::string>(&a);
	int ivalue = p.get<A, int>(&a);
	const property& pc = registry::get<A>("const_value");
	if (!pc) return;
	float const_value = pc.get<A, float>(&a);
	const property& pb = registry::get<A>("bvalue");
	pb.set(&a, 5);
	int bv = pb.get<A, int>(&a);

	registry::purge();
}

class TestData
{
public:
	TestData() : x(1999), tag("a tag") {}

	TestData(int x, std::string tag) : x(x), tag(tag) {}

	TestData(const TestData & other) : x(other.x), tag(other.tag) { }

	TestData & operator = (const TestData & other) {
		this->x = other.x;
		this->tag = other.tag;
		return *this;
	}

	bool operator == (const TestData & other) const {
		return this->x == other.x
			&& this->tag == other.tag;
	}

public:
	int x;
	std::string tag;
};

class TestNoncopyable
{
public:
	TestNoncopyable() : x(1999), tag("a tag") {}


	TestNoncopyable(int x, const std::string & tag) : x(x), tag(tag) {}

	bool operator == (const TestNoncopyable & other) const {
		return this->x == other.x
			&& this->tag == other.tag;
	}

private:
	TestNoncopyable(const TestNoncopyable & other);
	TestNoncopyable & operator = (const TestNoncopyable & other);

public:
	int x;
	std::string tag;
};

class TestObject
{
public:
	TestObject()
		: width(10),
		name("what's the name"),
		data()
	{
	}

	TestObject(const TestObject & other)
		: width(other.width),
		name(other.name),
		data(other.data)
	{
	}

	TestObject & operator = (const TestObject & other) {
		this->width = other.width;
		this->name = other.name;
		this->data = other.data;

		return *this;
	}

	int operator () (const std::string & s) const {
		return this->width + (int)s.length();
	}

private:
	int width;
	std::string name;
	TestData data;
	static int stats;

	TestNoncopyable nocopy;

	cpgf::GCallback<int(const std::string &)> callback;

	friend void doTestLib();
	friend void doTestAPI();
	friend void fieldDefineClass();
};

void fieldDefineClass()
{
	using namespace cpgf;

	GDefineMetaClass<TestObject>
		::define("field::TestObject")

		._field("width", &TestObject::width)
		._field("name", &TestObject::name)
		._field("data", &TestObject::data)
		._field("callback", &TestObject::callback)
		._field("stats", &TestObject::stats)
		._field("nocopy", &TestObject::nocopy, GMetaPolicyNoncopyable())
		;
}

int TestObject::stats = 5;

inline void checkEnum(const cpgf::GMetaEnum * metaEnum, const char * prefix)
{
	using namespace cpgf;

	testCheckAssert(metaEnum != NULL);

	size_t valueCount = metaEnum->getCount();

	testCheckAssert(valueCount > 0);

	int startValue = fromVariant<int>(metaEnum->getValue(0));
	int endValue = fromVariant<int>(metaEnum->getValue(valueCount - 1));
	size_t step = valueCount == 1 ? 0 : (endValue - startValue) / (valueCount - 1);
	testCheckEqual(fromVariant<int>(metaEnum->getValue(valueCount - 1)), static_cast<int>(startValue + (valueCount - 1) * step));

	char name[100];
	for (size_t i = 0; i < valueCount; ++i) {
		sprintf(name, "%s%d", prefix, static_cast<int>(i));
		testCheckStringEqual(metaEnum->getKey(i), name);
		testCheckEqual(fromVariant<int>(metaEnum->getValue(i)), static_cast<int>(startValue + i * step));
	}
}

template <typename Meta>
inline void checkEnum(Meta & metaEnum, const char * prefix)
{
	using namespace cpgf;

	testCheckAssert(metaEnum);

	size_t valueCount = metaEnum->getCount();

	testCheckAssert(valueCount > 0);

	int startValue = fromVariant<int>(metaGetEnumValue(metaEnum.get(), 0));
	int endValue = fromVariant<int>(metaGetEnumValue(metaEnum.get(), valueCount - 1));
	size_t step = valueCount == 1 ? 0 : (endValue - startValue) / (valueCount - 1);
	testCheckEqual(fromVariant<int>(metaGetEnumValue(metaEnum.get(), valueCount - 1)), static_cast<int>(startValue + (valueCount - 1) * step));

	char name[100];
	for (unsigned int i = 0; i < valueCount; ++i) {
		sprintf(name, "%s%d", prefix, i);
		testCheckStringEqual(metaEnum->getKey(i), name);
		testCheckEqual(fromVariant<int>(metaGetEnumValue(metaEnum.get(), i)), static_cast<int>(startValue + i * step));
	}
}

//INT WINAPI wWinMain( HINSTANCE, HINSTANCE, LPWSTR params, int )
int main()
{
	fieldDefineClass();
	
    // Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
    _CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	//_CrtSetBreakAlloc(9799);
#endif
	
	test_props();

	//{
	//	SensorsApp app;
	//	app.run();
	//}

    return 0;
}