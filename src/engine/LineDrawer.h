#pragma once

#include "render_gl.h"

namespace utility
{
	class Canvas
	{
	public:
		Canvas();
		~Canvas();

		enum fill_mode {
			NONE = 0,
			FILL, 
			OUTLINE,		
			FILL_UNKNOWN
		};

		void add( const glm::vec3& p1, const glm::vec3& p2, unsigned long color = 0xffffffff ) {
			add(p1, p1, p2, color, NONE);
		}

		void add_lines( const glm::vec3* p, size_t num, unsigned long color = 0xffffffff ) {
			for (size_t i = 0; i < num; i += 2) {
				add(p[i], p[i], p[i+1], color, NONE);
			}
		}
		
		void add( const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3, unsigned long color = 0xffffffff, fill_mode mode = FILL );
		
		void add_triangles( const glm::vec3* v, size_t num, unsigned long color = 0xffffffff, fill_mode mode = FILL ) {
			for (size_t i = 0; i < num; i += 3) {
				add(v[i], v[i+1], v[i+2], color, mode);
			}
		}

		//Helpers
		void grid3d(float size = 10.0f, float step = 0.25f);
		void cube(glm::vec3 offset, float scale, unsigned int color, fill_mode mode = NONE);
		void cone(glm::vec3 org, glm::vec3 r, glm::vec3 l, unsigned int color, fill_mode mode = NONE);
		void circle(glm::vec3 orig, glm::vec3 rx, glm::vec3 ry, float start, float end, unsigned int color, fill_mode mode = NONE);
		void axis3d();
		void point(const glm::vec3& p);

		void grid2d(float size = 800.0f, float step = 10.f);

		void render(const glm::mat4& view, const glm::mat4& proj);
		void render2d(float x, float y, float w, float h);

		void clean();

	private:		
		void update();

	protected:
		struct vertex_t {
			glm::vec3		pos;
			unsigned long	color;
		};

		struct batch_t {
			size_t start, num; 
			fill_mode mode;
		};

		std::vector<batch_t> batches;

		typedef std::vector<vertex_t> vertices_t;
		typedef vertices_t::iterator vertices_iter;

		vertices_t m_vertices;
		size_t m_reserved_size;

		mesh_ptr m_mesh;
		gpu_program_ptr shader;
	};

}