#pragma once

class cstr {
	void* p;
	size_t offset;
	size_t size;
public:
	cstr() : p(0), offset(0), size(0){}
	cstr(void* p, size_t offset, size_t size)
		: p(p), offset(offset), size(size) {}

	bool empty() const {return size == 0;}
	const char* c_str() const {return (const char*)p+offset;}
	char operator[](size_t i) {return c_str()[i];}
};

struct Skeleton {
	uint16_t num_joints; // total
	uint16_t *hierarchy; //parent index
	size_t *name_offsets;
	size_t *name_lengths;
	glm::mat4 *inv_bind_poses; 
	glm::mat4 *local_poses;
	glm::mat4 *global_poses;


	void LocalToGlobalPose(void);
};


void LocalToGlobalPose(void)
{
	// the root has no parent
	global_poses[0] = local_poses[0];

	for (uint16_t i = 1; i < num_joints; ++i) {
		const uint16_t parentJoint = hierarchy[i];
		global_poses[i] = global_poses[parentJoint] * local_poses[i];
	}
}
