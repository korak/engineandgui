#pragma once

namespace shapes {

	struct vertex {
		glm::vec3 pos;
		glm::vec3 norm;
	};

	static const vertex_atrib va[] = {
		{0, 3, GL_FLOAT, false, sizeof(vertex), 0},
		{1, 3, GL_FLOAT, true, sizeof(vertex), sizeof(glm::vec3)},
		{-1}
	};

	mesh_ptr create_box(float width = 1, float height = 1, float depth = 1);
	mesh_ptr create_cylinder(float radius1 = 1, float radius2 = 1, float length = 1, size_t slices = 8, size_t stacks = 8);
	mesh_ptr create_sphere(float radius = 1, size_t slices = 8, size_t stacks = 8);
	mesh_ptr create_teapot();
	mesh_ptr create_torus( float fInnerRadius = 1.0f, float fOuterRadius = 2.0f, size_t uSides = 8, size_t uRings = 15);
	mesh_ptr create_polygon(float length = 1, size_t sides = 3);
}