#include "stdafx.h"

#include "render_gl.h"
#include "shapes.h"

#define CACHE_SIZE    240

void sincosf(float x, float *sin, float *cos) {
	if (sin) *sin = sinf(x);
	if (cos) *cos = cosf(x);
}

namespace shapes 
{
	mesh_ptr create_mesh(vertex* vertices, size_t num_verts, unsigned short* indices, size_t num_indeces )
	{
		vb_ptr vb = VertexBuffer::create(sizeof(vertex)*num_verts, vertices);
		ib_ptr ib = IndexBuffer::create(sizeof(unsigned short)*num_indeces, indices);
		mesh_ptr mesh = Mesh::create(va, vb, ib);
		return mesh;
	}

	const float PI = 3.1415926535897932384626433832795f;
	#include "box.h"
	#include "cylinder.h"
	#include "sphere.h"
	#include "teapot.h"
	#include "torus.h"
	#include "polygon.h"

}