// Developed by Neil Kemp: http://neilkemp.us
// Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php


#ifndef ___GLL_SHADER_H___
#define ___GLL_SHADER_H___

#include "windows.h"
#include "gll_texture.h"

// Possible return values for load / create shader
#define GLL_SHADER_FAILED_ERROR				0
#define GLL_SHADER_FAILED_NOSUPPORT			1
#define GLL_SHADER_FAILED_COMPILE_ERROR		2
#define GLL_SHADER_SUCCESS					3

class gll_shader
{
private:

	unsigned int			___vs_handle;
	unsigned int			___fs_handle;
	unsigned int			___program_handle;

	BOOL					___is_set;

public:

	gll_shader(void);
	~gll_shader(void);

	unsigned int			load_from_file(wchar_t* vs_filename, wchar_t* fs_filename);
	unsigned int			create_from_source(const char* vs_source, const char* fs_source);
	
	void					free(void);

	void					set_shader(void);
	void					unset_shader(void);

	unsigned int			get_max_vs_shader_model(void) const;
	unsigned int			get_max_fs_shader_model(void) const;

	BOOL					set_uniform_int(const char* name, int i);
	BOOL					set_uniform_float(const char* name, float r);
	BOOL					set_uniform_vector(const char* name, const float *v, unsigned int size);
	BOOL					set_uniform_matrix(const char* name, const float *m, unsigned int size);

	BOOL					set_uniform_texture(const char* name, gll_texture &texture, 
												unsigned int texture_unit); 
};

#endif
