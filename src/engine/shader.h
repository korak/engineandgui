#pragma once

#include "render_gl.h"
#include <memory>

typedef std::shared_ptr<class Shader> Shader_ptr;


enum ShaderValueTypes {

};

struct ShaderParam {
	typedef std::weak_ptr<class Shader> ParentRef_t;
	ParentRef_t shader;

	std::string name;

	ShaderValueTypes type;

	struct ProgDesc {
		unsigned int prog_index;
		unsigned int uniform_id;
	};

	std::vector<ProgDesc> programs;
};

class Shader {
	Shader();
	Shader(const Shader&);	
public:
	Shader_ptr create(const char* filename);
		
	typedef std::map<std::string, gpu_program_ptr> stages_t;
	typedef std::map<std::string, ShaderParam> params_t;

	stages_t stages;
	params_t params;
};