#pragma once

namespace box {

static float cubeN[6][3] =
{
	{-1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {1.0f, 0.0f, 0.0f},
	{0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 1.0f}, {0.0f, 0.0f, -1.0f}
};

static unsigned short cubeF[6][4] =
{
	{ 0, 1, 5, 4 }, { 4, 5, 6, 7 }, { 7, 6, 2, 3 },
	{ 1, 0, 3, 2 }, { 1, 2, 6, 5 }, { 0, 4, 7, 3 }
};

static float cubeV[8][3] =
{
	// Lower tier (lower in y)
	{-.5f, -.5f, -.5f},
	{-.5f, -.5f,  .5f},
	{ .5f, -.5f,  .5f},
	{ .5f, -.5f, -.5f},

	// Upper tier
	{-.5f, .5f, -.5f},
	{-.5f, .5f,  .5f},
	{ .5f, .5f,  .5f},
	{ .5f, .5f, -.5f},
};

static float cubeT[4][2] =
{
	// Lower tier (lower in y)
	{0.0f, 0.0f},
	{0.0f, 1.0f},
	{1.0f, 1.0f},
	{1.0f, 0.0f}
};

static unsigned short cubeFT[6][4] =
{
	{ 3, 0, 1, 2 }, { 0, 1, 2, 3 }, { 1, 2, 3, 0 },
	{ 0, 1, 2, 3 }, { 3, 0, 1, 2 }, { 0, 1, 2, 3 }
};


void Generate( vertex* pVertices, unsigned short* pwIndices, float width, float height, float depth )
{
	// Fill in the data
	vertex* pVertex = pVertices;
	unsigned short* pwFace = pwIndices;
	unsigned short iVertex = 0;
	using namespace box;
	// i iterates over the faces, 2 triangles per face
	for( int i = 0; i < 6; i++ )
	{
		for( int j = 0; j < 4; j++ )
		{
			pVertex->pos.x = cubeV[cubeF[i][j]][0] * width;
			pVertex->pos.y = cubeV[cubeF[i][j]][1] * height;
			pVertex->pos.z = cubeV[cubeF[i][j]][2] * depth;

			pVertex->norm.x = cubeN[i][0];
			pVertex->norm.y = cubeN[i][1];
			pVertex->norm.z = cubeN[i][2];

			pVertex++;
		}

		pwFace[0] = iVertex;
		pwFace[1] = iVertex + 1;
		pwFace[2] = iVertex + 2;
		pwFace += 3;

		pwFace[0] = iVertex + 2;
		pwFace[1] = iVertex + 3;
		pwFace[2] = iVertex;
		pwFace += 3;

		iVertex += 4;
	}
}

}

mesh_ptr create_box(float width, float height, float depth) {
	const size_t cFaces = 12;
	const size_t cVertices = 24;

	unsigned short indices[cFaces*3];
	vertex vertices[cVertices];
	box::Generate(vertices, indices, width, height, depth);

	//mesh_ptr mesh = mesh::create(va);
	//mesh->update_ib(sizeof(indices), indices);
	//mesh->update_vb(sizeof(vertices), vertices);
	//return mesh;
	return create_mesh(vertices, cVertices, indices, cFaces*3);
}