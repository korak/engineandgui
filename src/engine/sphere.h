#pragma once

void MakeSphere( vertex* pVertices, unsigned short* pwIndices, float fRadius, size_t uSlices, size_t uStacks )
{
    size_t i, j;


    // Sin/Cos caches
    float sinI[CACHE_SIZE], cosI[CACHE_SIZE];
    float sinJ[CACHE_SIZE], cosJ[CACHE_SIZE];

    for( i = 0; i < uSlices; i++ )
        sincosf( 2.0f * PI * i / uSlices, sinI + i, cosI + i );

    for( j = 0; j < uStacks; j++ )
        sincosf( PI * j / uStacks, sinJ + j, cosJ + j );



    // Generate vertices
    vertex* pVertex = pVertices;

    // +Z pole
    pVertex->pos = glm::vec3( 0.0f, 0.0f, fRadius );
    pVertex->norm = glm::vec3( 0.0f, 0.0f, 1.0f );
    pVertex++;

    // Stacks
    for( j = 1; j < uStacks; j++ )
    {
        for( i = 0; i < uSlices; i++ )
        {
            glm::vec3 norm( sinI[i]* sinJ[j], cosI[i]* sinJ[j], cosJ[j] );

            pVertex->pos = norm * fRadius;
            pVertex->norm = norm;

            pVertex++;
        }
    }

    // Z- pole
    pVertex->pos = glm::vec3( 0.0f, 0.0f, -fRadius );
    pVertex->norm = glm::vec3( 0.0f, 0.0f, -1.0f );
    pVertex++;



    // Generate indices
    unsigned short* pwFace = pwIndices;
    size_t uRowA, uRowB;

    // Z+ pole
    uRowA = 0;
    uRowB = 1;

    for( i = 0; i < uSlices - 1; i++ )
    {
        pwFace[0] = ( uRowA );
        pwFace[1] = ( uRowB + i + 1 );
        pwFace[2] = ( uRowB + i );
        pwFace += 3;
    }

    pwFace[0] = ( uRowA );
    pwFace[1] = ( uRowB );
    pwFace[2] = ( uRowB + i );
    pwFace += 3;

    // Interior stacks
    for( j = 1; j < uStacks - 1; j++ )
    {
        uRowA = 1 + ( j - 1 ) * uSlices;
        uRowB = uRowA + uSlices;

        for( i = 0; i < uSlices - 1; i++ )
        {
            pwFace[0] = ( uRowA + i );
            pwFace[1] = ( uRowA + i + 1 );
            pwFace[2] = ( uRowB + i );
            pwFace += 3;

            pwFace[0] = ( uRowA + i + 1 );
            pwFace[1] = ( uRowB + i + 1 );
            pwFace[2] = ( uRowB + i );
            pwFace += 3;
        }

        pwFace[0] = ( uRowA + i );
        pwFace[1] = ( uRowA );
        pwFace[2] = ( uRowB + i );
        pwFace += 3;

        pwFace[0] = ( uRowA );
        pwFace[1] = ( uRowB );
        pwFace[2] = ( uRowB + i );
        pwFace += 3;
    }

    // Z- pole
    uRowA = 1 + ( uStacks - 2 ) * uSlices;
    uRowB = uRowA + uSlices;

    for( i = 0; i < uSlices - 1; i++ )
    {
        pwFace[0] = ( uRowA + i );
        pwFace[1] = ( uRowA + i + 1 );
        pwFace[2] = ( uRowB );
        pwFace += 3;
    }

    pwFace[0] = ( uRowA + i );
    pwFace[1] = ( uRowA );
    pwFace[2] = ( uRowB );
    pwFace += 3;
}

mesh_ptr create_sphere(float fRadius, size_t uSlices, size_t uStacks)
{
	unsigned short* pwIndices = NULL;
	vertex* pVertices = NULL;

	// Validate parameters
	if( fRadius < 0.0f ) return mesh_ptr();
	if( uSlices < 2 ) return mesh_ptr(); 
	if( uStacks < 2 ) return mesh_ptr();

	if( uSlices > CACHE_SIZE )
		uSlices = CACHE_SIZE;
	if( uStacks > CACHE_SIZE )
		uStacks = CACHE_SIZE;

	// Create the mesh
	const size_t cFaces = 2 * ( uStacks - 1 ) * uSlices;
	const size_t cVertices = ( uStacks - 1 ) * uSlices + 2;

	// Create enough memory for the vertices and indices
	pVertices = new vertex[ cVertices ];
	if( !pVertices ) return mesh_ptr();
	pwIndices = new unsigned short[ cFaces * 3 ];
	if( !pwIndices ) {
		delete [] pVertices;
		return mesh_ptr();
	}

	// Create a sphere
	MakeSphere( pVertices, pwIndices, fRadius, uSlices, uStacks );

	// Create a mesh
	mesh_ptr mesh = create_mesh(pVertices, cVertices, pwIndices, cFaces * 3 );

	// Free up the memory
	delete [] pVertices;
	delete [] pwIndices;

	return mesh;
}
