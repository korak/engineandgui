// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

#pragma warning(disable : 4561)	//	__fastcall' incompatible with the '/clr' option: converting to '__stdcall
#pragma warning(disable : 4251) // class XXX needs to have dll-interface to be used by clients of class YYY
#pragma warning(disable : 4793) // XXX : : causes native code generation for function YYYY

// TODO: reference additional headers your program requires here
#include <map>
#include <list>
#include <string>
#include <vector>
#include <bitset>
#include <hash_map>
#include <exception>

#include <windows.h>
#include <GLFW/glfw3.h>

#undef ERROR
#include <glm/glm.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/spline.hpp>
#include <glm/gtc/type_ptr.hpp>