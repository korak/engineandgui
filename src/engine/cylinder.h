#pragma once

namespace cylinder {

void Generate( vertex* pVertices, unsigned short* pPointReps, unsigned short* pwIndices, float fRadius1, 
	float fRadius2, float fLength, size_t uSlices, size_t uStacks )
{
	UINT i, j;

	// Sin/Cos caches
	float sinI[CACHE_SIZE], cosI[CACHE_SIZE];

	for( i = 0; i < uSlices; i++ )
		sincosf( 2.0f * PI * i / uSlices, sinI + i, cosI + i );


	// Compute side normal angle
	float fDeltaRadius = fRadius2 - fRadius1;
	float fSideLength = sqrtf( fDeltaRadius * fDeltaRadius + fLength * fLength );

	float fNormalXY = ( fSideLength > 0.00001f ) ? ( fLength / fSideLength )       : 1.0f;
	float fNormalZ = ( fSideLength > 0.00001f ) ? ( -fDeltaRadius / fSideLength ) : 0.0f;

	// Generate vertices
	vertex* pVertex = pVertices;
	float fZ, fRadius;
	unsigned short iVertex;

	// Base cap (uSlices + 1)
	fZ = fLength * -0.5f;
	fRadius = fRadius1;
	iVertex = 0;

	pVertex->pos = glm::vec3( 0.0f, 0.0f, fZ );
	pVertex->norm = glm::vec3( 0.0f, 0.0f, -1.0f );
	pVertex++;
	if( pPointReps != NULL )
		pPointReps[iVertex] = iVertex;
	iVertex++;

	for( i = 0; i < uSlices; i++ )
	{
		pVertex->pos = glm::vec3( fRadius * sinI[i], fRadius * cosI[i], fZ );
		pVertex->norm = glm::vec3( 0.0f, 0.0f, -1.0f );
		pVertex++;

		// link into stack vertices, which follow
		if( pPointReps != NULL )
			pPointReps[iVertex] = iVertex + uSlices;
		iVertex++;
	}

	// Stacks ((uStacks + 1)*uSlices)
	for( j = 0; j <= uStacks; j++ )
	{
		float f = ( float )j / ( float )uStacks;

		fZ = fLength * ( f - 0.5f );
		fRadius = fRadius1 + f * fDeltaRadius;

		for( i = 0; i < uSlices; i++ )
		{
			pVertex->pos = glm::vec3( fRadius * sinI[i], fRadius * cosI[i], fZ );
			pVertex->norm = glm::vec3( fNormalXY * sinI[i], fNormalXY * cosI[i], fNormalZ );
			pVertex++;
			if( pPointReps != NULL )
				pPointReps[iVertex] = iVertex;
			iVertex++;
		}
	}

	// Top cap (uSlices + 1)
	fZ = fLength * 0.5f;
	fRadius = fRadius2;

	for( i = 0; i < uSlices; i++ )
	{
		pVertex->pos = glm::vec3( fRadius * sinI[i], fRadius * cosI[i], fZ );
		pVertex->norm = glm::vec3( 0.0f, 0.0f, 1.0f );
		pVertex++;

		// link into stack vertices, which precede
		if( pPointReps != NULL )
			pPointReps[iVertex] = iVertex - uSlices;
		iVertex++;
	}

	pVertex->pos = glm::vec3( 0.0f, 0.0f, fZ );
	pVertex->norm = glm::vec3( 0.0f, 0.0f, 1.0f );
	pVertex++;
	if( pPointReps != NULL )
		pPointReps[iVertex] = iVertex;
	iVertex++;

	// Generate indices
	unsigned short* pwFace = pwIndices;
	unsigned int uRowA, uRowB;

	// Z+ pole (uSlices)
	uRowA = 0;
	uRowB = 1;

	for( i = 0; i < uSlices - 1; i++ )
	{
		pwFace[0] = ( uRowA );
		pwFace[1] = ( uRowB + i );
		pwFace[2] = ( uRowB + i + 1 );
		pwFace += 3;
	}

	pwFace[0] = ( uRowA );
	pwFace[1] = ( uRowB + i );
	pwFace[2] = ( uRowB );
	pwFace += 3;

	// Interior stacks (uStacks * uSlices * 2)
	for( j = 0; j < uStacks; j++ )
	{
		uRowA = 1 + ( j + 1 ) * uSlices;
		uRowB = uRowA + uSlices;

		for( i = 0; i < uSlices - 1; i++ )
		{
			pwFace[0] = ( uRowA + i );
			pwFace[1] = ( uRowB + i );
			pwFace[2] = ( uRowA + i + 1 );
			pwFace += 3;

			pwFace[0] = ( uRowA + i + 1 );
			pwFace[1] = ( uRowB + i );
			pwFace[2] = ( uRowB + i + 1 );
			pwFace += 3;
		}

		pwFace[0] = ( uRowA + i );
		pwFace[1] = ( uRowB + i );
		pwFace[2] = ( uRowA );
		pwFace += 3;

		pwFace[0] = ( uRowA );
		pwFace[1] = ( uRowB + i );
		pwFace[2] = ( uRowB );
		pwFace += 3;
	}

	// Z- pole (uSlices)
	uRowA = 1 + ( uStacks + 2 ) * uSlices;
	uRowB = uRowA + uSlices;

	for( i = 0; i < uSlices - 1; i++ )
	{
		pwFace[0] = ( uRowA + i );
		pwFace[1] = ( uRowB );
		pwFace[2] = ( uRowA + i + 1 );
		pwFace += 3;
	}

	pwFace[0] = ( uRowA + i );
	pwFace[1] = ( uRowB );
	pwFace[2] = ( uRowA );
	pwFace += 3;
}

}

mesh_ptr create_cylinder(float fRadius1, float fRadius2, float fLength, size_t uSlices, size_t uStacks)
{
	unsigned short* pwIndices = NULL;
	vertex* pVertices = NULL;

	// Set up the defaults
	//if( D3DX_DEFAULT_FLOAT == fRadius1 )
	//	fRadius1 = 1.0f;
	//if( D3DX_DEFAULT_FLOAT == fRadius2 )
	//	fRadius2 = 1.0f;
	//if( D3DX_DEFAULT_FLOAT == fLength )
	//	fLength = 1.0f;
	//if( D3DX_DEFAULT == uSlices )
	//	uSlices = 8;
	//if( D3DX_DEFAULT == uStacks )
	//	uStacks = 8;


	// Validate parameters
	if( fRadius1 < 0.0f ) return mesh_ptr();
	if( fRadius2 < 0.0f )return mesh_ptr();
	if( fLength < 0.0f )return mesh_ptr();
	if( uSlices < 2 ) return mesh_ptr();
	if( uStacks < 1 ) return mesh_ptr();
	if( uSlices >= CACHE_SIZE )	return mesh_ptr();

	// Create the mesh
	const size_t cFaces = ( uStacks + 1 ) * uSlices * 2;
	const size_t cVertices = 2 + ( uStacks + 3 ) * uSlices;

	// Create enough memory for the vertices and indices
	pVertices = new vertex[ cVertices ];
	if( !pVertices ) return mesh_ptr();
	pwIndices = new unsigned short[ cFaces * 3 ];
	if( !pwIndices ) {
		delete [] pVertices;
		return mesh_ptr();
	}


	// Create a cylinder
	cylinder::Generate( pVertices, NULL, pwIndices, fRadius1, fRadius2, fLength, uSlices, uStacks );

	// Create a mesh
	mesh_ptr mesh = create_mesh(pVertices, cVertices, pwIndices, cFaces * 3 );

	// Free up the memory
	delete [] pVertices;
	delete [] pwIndices;

	return mesh;
}
