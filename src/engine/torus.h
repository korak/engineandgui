#pragma once


//---------------------------------------------------------------------
// MakeTorus helper
//---------------------------------------------------------------------
static void MakeTorus( vertex* pVertices, unsigned short* pwIndices, float fInnerRadius, float fOuterRadius, size_t uSides, size_t uRings )
{
	size_t i, j;

	//
	// Compute the vertices
	//

	vertex* pVertex = pVertices;

	for( i = 0; i < uRings; i++ )
	{
		float theta = ( float )i * 2.0f * PI / ( float )uRings;
		float st, ct;

		sincosf( theta, &st, &ct );

		for( j = 0; j < uSides; j++ )
		{
			float phi = ( float )j * 2.0f * PI / uSides;
			float sp, cp;

			sincosf( phi, &sp, &cp );

			pVertex->pos.x = ct * ( fOuterRadius + fInnerRadius * cp );
			pVertex->pos.y = -st * ( fOuterRadius + fInnerRadius * cp );
			pVertex->pos.z = sp * fInnerRadius;

			pVertex->norm.x = ct * cp;
			pVertex->norm.y = -st * cp;
			pVertex->norm.z = sp;

			pVertex++;
		}
	}

	//
	// Compute the indices: 
	// There are uRings * uSides faces
	// Each face has 2 triangles (6 indices)
	//

	// Tube i has indices:  
	//        Left Edge: i*(uSides+1) -- i*(uSides+1)+uSides
	//        Right Edge: (i+1)*(uSides+1) -- (i+1)*(uSides+1)+uSides
	//
	// Face j on tube i has the 4 indices:
	//        Left Edge: i*(uSides+1)+j -- i*(uSides+1)+j+1
	//        Right Edge: (i+1)*(uSides+1)+j -- (i+1)*(uSides+1)+j+1
	//
	unsigned short* pwFace = pwIndices;

	for( i = 0; i < uRings - 1; i++ )
	{
		for( j = 0; j < uSides - 1; j++ )
		{

			// Tri 1 (Top-Left tri, CCW)
			pwFace[0] = ( i * uSides + j );
			pwFace[1] = ( i * uSides + j + 1 );
			pwFace[2] = ( ( i + 1 ) * uSides + j );
			pwFace += 3;

			// Tri 2 (Bottom-Right tri, CCW)
			pwFace[0] = ( ( i + 1 ) * uSides + j );
			pwFace[1] = ( i * uSides + j + 1 );
			pwFace[2] = ( ( i + 1 ) * uSides + j + 1 );
			pwFace += 3;
		}

		// Tri 1 (Top-Left tri, CCW)
		pwFace[0] = ( i * uSides + j );
		pwFace[1] = ( i * uSides );
		pwFace[2] = ( ( i + 1 ) * uSides + j );
		pwFace += 3;

		// Tri 2 (Bottom-Right tri, CCW)
		pwFace[0] = ( ( i + 1 ) * uSides + j );
		pwFace[1] = ( i * uSides + 0 );
		pwFace[2] = ( ( i + 1 ) * uSides + 0 );
		pwFace += 3;
	}


	// join the two ends of the tube
	for( j = 0; j < uSides - 1; j++ )
	{
		// Tri 1 (Top-Left tri, CCW)
		pwFace[0] = ( i * uSides + j );
		pwFace[1] = ( i * uSides + j + 1 );
		pwFace[2] = ( j );
		pwFace += 3;

		// Tri 2 (Bottom-Right tri, CCW)
		pwFace[0] = ( j );
		pwFace[1] = ( i * uSides + j + 1 );
		pwFace[2] = ( j + 1 );
		pwFace += 3;
	}

	// Tri 1 (Top-Left tri, CCW)
	pwFace[0] = ( i * uSides + j );
	pwFace[1] = ( i * uSides );
	pwFace[2] = ( j );
	pwFace += 3;

	// Tri 2 (Bottom-Right tri, CCW)
	pwFace[0] = ( j );
	pwFace[1] = ( i * uSides );
	pwFace[2] = ( 0 );
	pwFace += 3;
}

//----------------------------------------------------------------------------
// DXUTCreateTorus - create a torus mesh
//----------------------------------------------------------------------------
mesh_ptr create_torus( float fInnerRadius, float fOuterRadius, size_t uSides, size_t uRings)
{
	unsigned short* pwIndices = NULL;
	vertex* pVertices = NULL;

	if( fInnerRadius < 0.0f )
		return mesh_ptr();
	if( fOuterRadius < 0.0f )
		return mesh_ptr();
	if( uSides < 3 )
		return mesh_ptr();
	if( uRings < 3 )
		return mesh_ptr();

	// Create the mesh
	const size_t cFaces = 2 * uSides * uRings;
	const size_t cVertices = uRings * uSides;

	// Create enough memory for the vertices and indices
	pVertices = new vertex[ cVertices ];
	if( !pVertices )
		return mesh_ptr();
	pwIndices = new unsigned short[ cFaces * 3 ];
	if( !pwIndices ) {
		delete [] pVertices;
		return mesh_ptr();
	}

	// Create a torus
	MakeTorus( pVertices, pwIndices, fInnerRadius, fOuterRadius,
		uSides, uRings );

	// Create a mesh
	mesh_ptr mesh = create_mesh(pVertices, cVertices, pwIndices, cFaces * 3 );

	// Free up the memory
	delete [] pVertices;
	delete [] pwIndices;

	return mesh;
}

