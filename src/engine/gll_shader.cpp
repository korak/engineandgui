// Developed by Neil Kemp: http://neilkemp.us
// Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php


#include "gll_shader.h"
#include "assert.h"
#include <gl\gl.h>
#include <gl\glu.h>
#include "glext.h"
#include "stdio.h"
#include "tchar.h"



// Local static variables
// ----------------------------------------------------------------
// ARB_shading_language_100
static PFNGLCREATEPROGRAMOBJECTARBPROC		glCreateProgramObjectARB		= 0;
static PFNGLDELETEOBJECTARBPROC				glDeleteObjectARB				= 0;
static PFNGLUSEPROGRAMOBJECTARBPROC			glUseProgramObjectARB			= 0;
static PFNGLCREATESHADEROBJECTARBPROC		glCreateShaderObjectARB			= 0;
static PFNGLSHADERSOURCEARBPROC				glShaderSourceARB				= 0;
static PFNGLCOMPILESHADERARBPROC			glCompileShaderARB				= 0;
static PFNGLGETOBJECTPARAMETERIVARBPROC		glGetObjectParameterivARB		= 0;
static PFNGLATTACHOBJECTARBPROC				glAttachObjectARB				= 0;
static PFNGLGETINFOLOGARBPROC				glGetInfoLogARB					= 0;
static PFNGLLINKPROGRAMARBPROC				glLinkProgramARB 				= 0;
static PFNGLGETUNIFORMLOCATIONARBPROC		glGetUniformLocationARB 		= 0;
static PFNGLUNIFORM4FARBPROC				glUniform4fARB					= 0;
static PFNGLUNIFORM3FARBPROC				glUniform3fARB					= 0; 
static PFNGLUNIFORM2FARBPROC				glUniform2fARB 					= 0;
static PFNGLUNIFORM1FARBPROC				glUniform1fARB 					= 0;
static PFNGLUNIFORM1IARBPROC				glUniform1iARB					= 0;
static PFNGLUNIFORMMATRIX2FVARBPROC			glUniformMatrix2fvARB			= 0;
static PFNGLUNIFORMMATRIX3FVARBPROC			glUniformMatrix3fvARB			= 0;
static PFNGLUNIFORMMATRIX4FVARBPROC			glUniformMatrix4fvARB			= 0;
static PFNGLGETATTRIBLOCATIONARBPROC		glGetAttribLocationARB			= 0;
static PFNGLVERTEXATTRIB3FARBPROC			glVertexAttrib3fARB				= 0;
static PFNGLVERTEXATTRIBPOINTERARBPROC      glVertexAttribPointerARB		= 0;
static PFNGLENABLEVERTEXATTRIBARRAYARBPROC  glEnableVertexAttribArrayARB	= 0;
static PFNGLDISABLEVERTEXATTRIBARRAYARBPROC glDisableVertexAttribArrayARB	= 0;


// Local helper functions
// ----------------------------------------------------------------
// Initialize shader extensions
BOOL local_initialize_shader_extensions(void)
{
	// Determine if function addresses haven't been assigned
	if(glCreateProgramObjectARB == 0)
	{
		//Create all the functions using wglGetProcAddress
		glCreateProgramObjectARB		= (PFNGLCREATEPROGRAMOBJECTARBPROC)wglGetProcAddress("glCreateProgramObjectARB");
		glDeleteObjectARB				= (PFNGLDELETEOBJECTARBPROC)wglGetProcAddress("glDeleteObjectARB");
		glUseProgramObjectARB			= (PFNGLUSEPROGRAMOBJECTARBPROC)wglGetProcAddress("glUseProgramObjectARB");
		glCreateShaderObjectARB			= (PFNGLCREATESHADEROBJECTARBPROC)wglGetProcAddress("glCreateShaderObjectARB");
		glShaderSourceARB				= (PFNGLSHADERSOURCEARBPROC)wglGetProcAddress("glShaderSourceARB"); 
		glCompileShaderARB				= (PFNGLCOMPILESHADERARBPROC)wglGetProcAddress("glCompileShaderARB"); 
		glGetObjectParameterivARB		= (PFNGLGETOBJECTPARAMETERIVARBPROC)wglGetProcAddress("glGetObjectParameterivARB"); 
		glAttachObjectARB				= (PFNGLATTACHOBJECTARBPROC)wglGetProcAddress("glAttachObjectARB"); 
		glGetInfoLogARB					= (PFNGLGETINFOLOGARBPROC)wglGetProcAddress("glGetInfoLogARB");
		glLinkProgramARB				= (PFNGLLINKPROGRAMARBPROC)wglGetProcAddress("glLinkProgramARB");
		glGetUniformLocationARB			= (PFNGLGETUNIFORMLOCATIONARBPROC)wglGetProcAddress("glGetUniformLocationARB");
		glUniform4fARB					= (PFNGLUNIFORM4FARBPROC)wglGetProcAddress("glUniform4fARB");
		glUniform3fARB					= (PFNGLUNIFORM3FARBPROC)wglGetProcAddress("glUniform3fARB");
		glUniform2fARB					= (PFNGLUNIFORM2FARBPROC)wglGetProcAddress("glUniform2fARB");
		glUniform1fARB					= (PFNGLUNIFORM1FARBPROC)wglGetProcAddress("glUniform1fARB");
		glUniform1iARB					= (PFNGLUNIFORM1IARBPROC)wglGetProcAddress("glUniform1iARB");
		glUniformMatrix2fvARB			= (PFNGLUNIFORMMATRIX2FVARBPROC)wglGetProcAddress("glUniformMatrix2fvARB");
		glUniformMatrix3fvARB			= (PFNGLUNIFORMMATRIX3FVARBPROC)wglGetProcAddress("glUniformMatrix3fvARB");
		glUniformMatrix4fvARB			= (PFNGLUNIFORMMATRIX4FVARBPROC)wglGetProcAddress("glUniformMatrix4fvARB");
		glGetAttribLocationARB			= (PFNGLGETATTRIBLOCATIONARBPROC)wglGetProcAddress("glGetAttribLocationARB");
		glVertexAttrib3fARB				= (PFNGLVERTEXATTRIB3FARBPROC)wglGetProcAddress("glVertexAttrib3fARB");
		glVertexAttribPointerARB		= (PFNGLVERTEXATTRIBPOINTERARBPROC)wglGetProcAddress("glVertexAttribPointerARB");
		glEnableVertexAttribArrayARB	= (PFNGLENABLEVERTEXATTRIBARRAYARBPROC)wglGetProcAddress("glEnableVertexAttribArrayARB");
		glDisableVertexAttribArrayARB	= (PFNGLDISABLEVERTEXATTRIBARRAYARBPROC)wglGetProcAddress("glDisableVertexAttribArrayARB");
	}

	if(!glCreateProgramObjectARB || !glDeleteObjectARB || !glUseProgramObjectARB || !glCreateShaderObjectARB ||
	   !glShaderSourceARB || !glCompileShaderARB || !glGetObjectParameterivARB || !glAttachObjectARB || 
	   !glGetInfoLogARB || !glLinkProgramARB || !glGetUniformLocationARB || !glUniform4fARB || !glUniform3fARB || 
	   !glUniform2fARB || !glUniform1fARB || !glUniform1iARB || !glUniformMatrix2fvARB || !glUniformMatrix3fvARB ||
	   !glUniformMatrix4fvARB || !glGetAttribLocationARB || !glVertexAttrib3fARB || !glVertexAttribPointerARB || 
	   !glEnableVertexAttribArrayARB || !glDisableVertexAttribArrayARB)
	{	return FALSE;
	}
	return TRUE;
}
// Return the size of a file in bytes
unsigned int get_file_size(const wchar_t *filename)
{
	// Local data
	unsigned int	size;
	FILE			*fp;

	_wfopen_s(&fp, filename, _T("rb"));
	if(!fp)
	{	return 0;
	}
	fseek(fp, 0, SEEK_END);
	size = ftell(fp);
	fclose(fp);
	return size;
}
// Is a gl extension supported
BOOL is_gl_extension_supported(const char *string)
{
	const unsigned char		*extensions_list;
	
	// Get extensions list
	extensions_list = 0;
	extensions_list = glGetString(GL_EXTENSIONS);
	// Check for shader ARB extension
	if(strstr((const char*)extensions_list, string) == 0)
    {	return FALSE;
    }
	return TRUE;
}


// OpenGL shader class Constructor / Destructor
// ----------------------------------------------------------------
// c()
gll_shader::gll_shader(void)
{
	___vs_handle		= 0;
	___fs_handle		= 0;
	___program_handle	= 0;

	___is_set			= FALSE;
}
// d()
gll_shader::~gll_shader(void)
{	
}

// OpenGL shader public member functions
// ----------------------------------------------------------------
// Load the shaders from file
unsigned int gll_shader::load_from_file(wchar_t* vs_filename, wchar_t* fs_filename)
{
	char			*vs_source = 0;
	char			*fs_source = 0;
	unsigned int	vs_file_size, fs_file_size;
	unsigned int	ret;
	FILE			*vs_fp, *fs_fp;

	// Get vertex shader file size
	vs_file_size = get_file_size(vs_filename);
	if(vs_file_size == 0)
	{	return FALSE;
	}
	// Get fragment shader file size
	fs_file_size = get_file_size(fs_filename);
	if(fs_file_size == 0)
	{	return FALSE;
	}
	// Allocate shader source memory
	vs_source = new char[vs_file_size];
	if(!vs_source)
	{	return FALSE;
	}
	fs_source = new char[fs_file_size];
	if(!fs_source)
	{	delete [] vs_source;
		return FALSE;
	}
	// Read in the vertex source
	_wfopen_s(&vs_fp, vs_filename, _T("rb"));
	if(!vs_fp)
	{	delete [] vs_source;
		delete [] fs_source;
		return FALSE;
	}
	fread(vs_source, vs_file_size, sizeof(char), vs_fp);
	vs_source[vs_file_size-1] = '\0';
	fclose(vs_fp);

	// Read in the fragment source
	_wfopen_s(&fs_fp, fs_filename, _T("rb"));	
	if(!fs_fp)
	{	delete [] vs_source;
		delete [] fs_source;
		return FALSE;
	}
	fread(fs_source, fs_file_size, sizeof(char), fs_fp);
	fs_source[fs_file_size-1] = '\0';
	fclose(fs_fp);

	// Return create from source function
	ret = create_from_source(vs_source, fs_source);	

	// Deallocate source strings
	delete [] vs_source;
	vs_source = 0;
	delete [] fs_source;
	fs_source = 0;

	return ret;
}
// Create the shader from source code
unsigned int gll_shader::create_from_source(const char* vs_source, const char* fs_source)
{
	// Local data
	const unsigned char		*extensions_list;
	char					info_buffer[1024];	
	int						info_buffer_length;

	// Free old shader
	free();

	// Get extensions list
	extensions_list = 0;
	extensions_list = glGetString(GL_EXTENSIONS);
	// Check for shader ARB extension
	if(!is_gl_extension_supported("ARB_shading_language_100"))
	{	return GLL_SHADER_FAILED_NOSUPPORT;
	}
	
	// Ensure shader extensions are initialized
	if(!local_initialize_shader_extensions())
	{	// ERROR
		return GLL_SHADER_FAILED_NOSUPPORT;
	}
	// Create the vertex shader	
	___vs_handle = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
	if(glGetError()!= GL_NO_ERROR)
	{	// ERROR
		MessageBoxA(0, (char*)gluErrorString(glGetError()), "Vertex Shader Error", MB_OK | MB_ICONEXCLAMATION);			
		return GLL_SHADER_FAILED_ERROR;
	}
	// Create the fragment shader
	___fs_handle = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
	if(glGetError()!= GL_NO_ERROR)
	{	// ERROR
		MessageBoxA(0, (char*)gluErrorString(glGetError()), "Fragment Shader Error", MB_OK | MB_ICONEXCLAMATION);
		return GLL_SHADER_FAILED_ERROR;
	}	
	// Set the source to the shader	
	glShaderSourceARB(___vs_handle, 1, &vs_source, 0);
	glShaderSourceARB(___fs_handle, 1, &fs_source, 0);

	// Compile the vs shader
	glCompileShaderARB(___vs_handle);
	// Check for vs errors
	info_buffer_length = 0;
	glGetInfoLogARB(___vs_handle, 1023, &info_buffer_length, info_buffer);
	info_buffer[info_buffer_length] = 0;
	if(strlen(info_buffer) != 0)
	{	// WARNING
		// MessageBoxA(0, info_buffer, "Vertex Shader Warning", MB_OK | MB_ICONEXCLAMATION);
	}

	// Compile the fs shader
	glCompileShaderARB(___fs_handle);
	// Check for fs errors
	info_buffer_length = 0;
	glGetInfoLogARB(___fs_handle, 1023, &info_buffer_length, info_buffer);
	info_buffer[info_buffer_length] = 0;
	if(strlen(info_buffer) != 0)
	{	// WARNING			
		// MessageBoxA(0, info_buffer, "Fragment Shader Warning", MB_OK | MB_ICONEXCLAMATION);
	}

	// Create the shader program
	___program_handle = glCreateProgramObjectARB();
	if(glGetError()!= GL_NO_ERROR)
	{	// ERROR
		MessageBoxA(0, (char*)gluErrorString(glGetError()), "Shader Program Error", MB_OK | MB_ICONEXCLAMATION);
		return GLL_SHADER_FAILED_ERROR;
	}

	// Link the program
	glAttachObjectARB(___program_handle,___vs_handle);
	glAttachObjectARB(___program_handle,___fs_handle);	
	glLinkProgramARB(___program_handle);
	// Check for linking errors
	info_buffer_length = 0;
	glGetInfoLogARB(___program_handle, 1023, &info_buffer_length, info_buffer);
	info_buffer[info_buffer_length] = 0;
	if(strlen(info_buffer) != 0)
	{	// WARNING	
		// MessageBoxA(0, info_buffer, "Shader Program Linking Warning", MB_OK | MB_ICONEXCLAMATION);
	}		
	
	return GLL_SHADER_SUCCESS;
}
// Release shader
void gll_shader::free(void)
{
	if(___program_handle)
	{
		glDeleteObjectARB(___vs_handle);
		glDeleteObjectARB(___fs_handle);
		glDeleteObjectARB(___program_handle);
		___vs_handle		= 0;
		___fs_handle		= 0;
		___program_handle	= 0;
		___is_set			= FALSE;
	}
}
// Set shader - don't call between glBegin and glEnd
void gll_shader::set_shader(void)
{	
	assert(___program_handle != 0);	

	// Set the shader
	glUseProgramObjectARB(___program_handle);
	// Remember it's set
	___is_set = TRUE;
}
// Unset the shader - don't call between glBegin and glEnd
void gll_shader::unset_shader(void)
{	// Unset the shader
	glUseProgramObjectARB(0);
	// Not set
	___is_set = FALSE;
}
// Return the maximum shader model supported by this system
unsigned int gll_shader::get_max_vs_shader_model(void) const
{
	double version = atof((const char*)glGetString(GL_VERSION));

	// Are shaders even supported
	if(!is_gl_extension_supported("ARB_shading_language_100"))
	{	return 0;
	}
	// Check for nVidia SM 4.0
	if(is_gl_extension_supported("GL_NV_vertex_program4"))
	{	return 4;
	}
	// Check for nVidia SM 3.0
	if(is_gl_extension_supported("GL_NV_vertex_program3"))
	{	return 3;
	}
	// Check for ATI SM 3.0
	if(is_gl_extension_supported("GL_ATI_shader_texture_lod"))
	{	return 3;
	}
	// Check for nVidia SM 2.0
	if(is_gl_extension_supported("GL_NV_vertex_program2"))
	{	return 2;
	}
	// Check if OpenGL 2.0 is supported and if yes then return 2
	else if(version >= 2.0)
	{	return 2;
	}
	return 1;
}
// Return the maximum shader model supported by this system
unsigned int gll_shader::get_max_fs_shader_model(void) const
{
	double version = atof((const char*)glGetString(GL_VERSION));

	// Are shaders even supported
	if(!is_gl_extension_supported("ARB_shading_language_100"))
	{	return 0;
	}
	// Check for nVidia SM 4.0
	if(is_gl_extension_supported("GL_NV_fragment_program4"))
	{	return 4;
	}
	// Check for nVidia SM 3.0
	if(is_gl_extension_supported("GL_NV_fragment_program3"))
	{	return 3;
	}
	// Check for ATI SM 3.0
	if(is_gl_extension_supported("GL_ATI_shader_texture_lod"))
	{	return 3;
	}
	// Check for nVidia SM 2.0
	if(is_gl_extension_supported("GL_NV_fragment_program2"))
	{	return 2;
	}
	// Check if OpenGL 2.0 is supported and if yes then return 2
	else if(version >= 2.0)
	{	return 2;
	}
	return 1;
}
// Set uniform integer
BOOL gll_shader::set_uniform_int(const char* name, int i)
{	
	// Assertions
	assert(___program_handle != 0);
	assert(___is_set != FALSE);
	
	// Get location
	int loc = glGetUniformLocationARB(___program_handle, name);
	if(loc == -1)
	{	return FALSE;
	}
	// Set integer at location
	glUniform1iARB(loc, i); 
	return TRUE;
}
// Set uniform float
BOOL gll_shader::set_uniform_float(const char* name, float r)
{
	// Assertions
	assert(___program_handle != 0);
	assert(___is_set != FALSE);	

	// Get location
	int loc = glGetUniformLocationARB(___program_handle, name);
	if(loc == -1)
	{	return FALSE;
	}
	// Set float at location
	glUniform1fARB(loc, r); 
	return TRUE;
}
// Set uniform vectors
BOOL gll_shader::set_uniform_vector(const char* name, const float *v, unsigned int size)
{
	// Assertions
	assert(___program_handle != 0);
	assert(___is_set != FALSE);	
	assert(size >= 2);
	assert(size <= 4);

	// Get location
	int loc = glGetUniformLocationARB(___program_handle, name);
	if(loc == -1)
	{	return FALSE;
	}
	// Set vector at location
	switch(size)
	{
	case 2:
		glUniform2fARB(loc, v[0], v[1]); 
		break;
	case 3:
		glUniform3fARB(loc, v[0], v[1], v[2]); 
		break;
	case 4:
		glUniform4fARB(loc, v[0], v[1], v[2], v[3]); 
		break;
	}
	return TRUE;
}
// Set uniform matrices
BOOL gll_shader::set_uniform_matrix(const char* name, const float *m, unsigned int size)
{
	// Assertions
	assert(___program_handle != 0);
	assert(___is_set != FALSE);
	assert(size >= 3);
	assert(size <= 4);

	// Get location
	int loc = glGetUniformLocationARB(___program_handle, name);
	if(loc == -1)
	{	return FALSE;
	}
	// Set float at location // FALSE to transpose or not
	switch(size)
	{
	case 3:
		glUniformMatrix3fvARB(loc, 1, FALSE, m);
		break;
	case 4:
		glUniformMatrix4fvARB(loc, 1, FALSE, m);
		break;
	}
	return TRUE;
}
// Set a uniform texture
BOOL gll_shader::set_uniform_texture(const char* name, gll_texture &texture,
									 unsigned int texture_unit)
{
	// Assertions
	assert(___program_handle != 0);
	assert(___is_set != FALSE);

	// Get location
	int loc = glGetUniformLocationARB(___program_handle, name);
	if(loc == -1)
	{	return FALSE;
	}
	// Bind the texture to location
	texture.bind_to_gl(texture_unit);
	// Set the texture unit to the location
	glUniform1iARB(loc, texture_unit);

	return TRUE;
}
