#pragma once

#include "render_gl.h"
#include "LibGizmo/inc/IGizmo.h"
#include <functional>


namespace events
{
	namespace details
	{
		class base_sender;
		class base_listener;
		class base_events_manager;

		class base_events_manager
		{
		protected:
			base_events_manager();
			virtual ~base_events_manager();

		public:
			virtual void unsubscribe (base_listener*) = 0;
			virtual void unsubscribe (base_listener*, void*) = 0;
		};
	}

	template <typename Event>
	class manager: public details::base_events_manager
	{
		struct subscription {
			details::base_listener *m_listener; 
			void *m_sender;
			std::function<void(Event)>  m_func;      //����� ����� ��������

			bool operator==(const subscription &s) {
				return m_listener == s.m_listener;
			}
		};

		typedef std::list<subscription> subscriptions_list;
		typedef std::map<void*, subscriptions_list> subcriptions_map;

	public:
		static manager& get() {
			static manager instance;
			return instance;
		}

		//��������� listener �� ��������� ������� � ������� func �� ����������� sender (���� ����� 0 - �� �� ����)
		void subscribe (details::base_listener *listener, std::function<void(Event)> func, void *sender) {
			subscription subs;
			subs.m_listener = listener;
			subs.m_func      = func;
			subs.m_sender   = sender;
			m_subscriptions[sender].push_back(subs);
		}

		void unsubscribe (details::base_listener *listener) {
			subcriptions_map::iterator mi = m_subscriptions.begin();
			for (;mi != m_subscriptions.end(); ++mi) {
				subscriptions_list &subs = mi->second;
				subscriptions_list::iterator i = subs.begin();
				for (;i != subs.end();++i) {
					if (i->m_listener == listener) {
						i = subs.erase(i);
						break;
					}
				}
			}
		}

		void unsubscribe (details::base_listener *listener, void *sender)
		{
			subscriptions_list& concrete_subs = m_subscriptions[sender];
			subscriptions_list::iterator i = concrete_subs.begin();
			for (;i != concrete_subs.end(); ++i) {
				if (i->m_listener == listener) {
					i = concrete_subs.erase(i);
					break;
				}
			}
		}

		//��������� ������� event �� ����������� sender
		//void send_event (const Event& event, const details::base_sender *sender)
		void send_event (const Event& event, void *sender)
		{
			subscriptions_list& broadcast_subs = m_subscriptions[0];
			if (!broadcast_subs.empty()) {
				subscriptions_list::iterator i = broadcast_subs.begin();
				for(;i != broadcast_subs.end();++i) {
					i->m_func(event);
				}
			}
			subscriptions_list& concrete_subs = m_subscriptions[sender];
			if (!concrete_subs.empty()) {
				subscriptions_list::iterator i = concrete_subs.begin();
				for(;i != concrete_subs.end();++i) {
					i->m_func(event);
				}
			}
		}

	private:
		manager () {}
		~manager() {}

		manager(const manager&);
		manager& operator= (const manager&);

		subcriptions_map m_subscriptions;
	};

	namespace details
	{
		class base_listener
		{
		protected:
			base_listener();
			virtual ~base_listener();

			template <typename Event>
			void subscribe( std::function<void(Event)> f, void *sender = 0) {
				manager<Event>::get().subscribe(this,f,sender);
			}

			template <typename Event>
			void unsubscribe(void *sender) {
				manager<Event>::get().unsubscribe(this, sender);
			}

			template <typename Event>
			void unsubscribe() {
				manager<Event>::get().unsubscribe(this);
			}

		private:
			base_listener(const base_listener&);
			base_listener& operator= (const base_listener&);
		};


		class base_sender
		{
		protected:
			base_sender();
			virtual ~base_sender();

			template<typename Event>
			void base_send_event(const Event& event) {
				manager<Event>::get().send_event(event,this);
			}

		private:
			base_sender(const base_sender&);
			base_sender& operator= (const base_sender&);
		};
	}

	class listener	: private details::base_listener
	{
	public:
		template <typename Event>
		void subscribe( std::function<void(Event)> func, details::base_sender *sender=0 ) {
			details::base_listener::subscribe<Event>(func,sender);
		}

		template<typename Event, typename Class, typename EventArg>
		void subscribe (void (Class::*ptr)(EventArg), void *sender=0) {
			details::base_listener::subscribe<Event>( 
				std::bind(ptr, static_cast<Class*>(this), std::placeholders::_1), sender );
		}

		template <typename Event>
		void unsubscribe(details::base_sender* sender) {
			details::base_listener::unsubscribe<Event>(sender);
		}

		template <typename Event>
		void unsubscribe() {
			details::base_listener::unsubscribe<Event>();
		}
	};

	class sender: public listener, public details::base_sender
	{
	public:
		template<typename Event>
		void send_event(const Event& event) {
			base_send_event(event);
		}
	};
}

template <class T>
class tree_node
{
public:
	typedef T node;
	typedef std::shared_ptr<node> node_ptr;
	typedef std::list<node_ptr> children_list;
	typedef typename children_list::const_iterator child_iter;

	void add(const node_ptr& node) {
		m_children.push_back(node); 
		node->parent((T*)this);
	}

	void remove(const node_ptr& node) {
		m_children.remove(node); 
		node->parent(0);
	}

	const node*		parent() const  {return m_parent;}
	node*			parent()		{return m_parent;}

	const std::string name() const	{return m_name;}
	void name(const std::string& s) {m_name = s;}

	const children_list& children() const {return m_children;}

	tree_node() : m_parent(0)
	{}

	virtual ~tree_node()
	{
		typedef children_list::iterator iter;		
		for (iter it = m_children.begin(); it != m_children.end(); ++it)
			(*it)->parent(0);

		// we'll do it explicit for easy debug
		m_children.clear();
	}

	void parent(node* ptr)	{m_parent = ptr; on_parent_changed();}

protected:				
	virtual void on_parent_changed(){}

protected:
	children_list	m_children;
	node*			m_parent;
	std::string		m_name;
};

class Entity;
typedef std::shared_ptr<Entity> entity_ptr;

enum entity_type {
	E_ACTOR, 
	E_CAMERA
};


class TrasformationFrame : public tree_node<TrasformationFrame>
{
public:
	static glm::mat4 make_transform(const glm::vec3& pos, const glm::quat& rot, const glm::vec3& s);
	static glm::mat4 make_transform(const glm::vec3& pos, const glm::vec3& rot, const glm::vec3& s);

	static TrasformationFrame*	create();				

	virtual				~TrasformationFrame();

	const glm::vec3&	position()		const {return m_position;}
	glm::vec3			world_position() const;
	void				position(const glm::vec3& pos);

	const glm::quat&	rotation()		const {return m_rotation;}
	void				rotation(const glm::quat& quat);

	void				lookat(const glm::vec3& eye, const glm::vec3& lookat, const glm::vec3& up_vec);

	/// it must be set directly to shader params
	const glm::mat4&			local_trasform() const;
	const glm::mat4&			world_trasform()  const;

	inline const glm::vec3&		scale()	const { return m_scale;}
	void						scale(const glm::vec3& s);

	// directional vectors
	glm::vec3					up()	const;
	glm::vec3					at()	const;
	glm::vec3					left()	const;

	glm::vec3					world_up() const;
	glm::vec3					world_at() const;
	glm::vec3					world_left() const;

	virtual void				update( bool invalidate_transform = false );

	//Finds frames with names witch contains substring str_template + "_"
	//void find(const std::string& str_template, std::vector<frame_ptr>& container);

protected:
	TrasformationFrame();
	TrasformationFrame(const TrasformationFrame&) {}

	virtual void on_parent_change();

	bool is_dirty()			const {return m_need_recompute;}
	virtual void update_transform()	const;
	virtual void update_world_transform() const;

protected:
	glm::vec3				m_scale;
	glm::vec3				m_position;
	glm::quat				m_rotation;

	mutable bool			m_need_recompute;
	mutable bool			m_recompute_global_matrix;

	mutable glm::mat4		m_local_tm;
	mutable glm::mat4		m_fullTransform;
};


class Entity : public tree_node<Entity> {
public:
	typedef std::vector<entity_ptr> children_t;

	explicit Entity(const char* name = 0) 
		: m_position(0), m_scale(1), m_rotation(glm::vec3(0,0,0)), is_dirty(true), m_type_id(0) 
	{
		if (name) m_name = name;
	}

	const glm::vec3& position() const {return m_position;}
	void position(const glm::vec3& p) {m_position = p; is_dirty = true;}

	const glm::vec3& scale() const {return m_scale;}
	void scale(const glm::vec3& s) {m_scale = s; is_dirty = true;}

	const glm::quat& rotation() const {return m_rotation;}
	void rotation(const glm::quat& r) {m_rotation = r; is_dirty = true;}

	size_t type_id() const {return m_type_id;}

	glm::mat4& local_tm()  {if (is_dirty) update_matrix(); return m_local_transform;}
	const glm::mat4& local_tm() const {return m_local_transform;}

protected:
	virtual void update_matrix();

	glm::quat m_rotation;
	glm::vec3 m_scale;
	glm::vec3 m_position;

	bool is_dirty; // need to update local transform
	glm::mat4 m_local_transform;

	size_t m_type_id; // for custom RTTID
};

class Actor : public Entity {
public:
	Actor()  {m_type_id = E_ACTOR;}
	virtual void update() = 0;
};

class Camera : public Entity
{
public:
	Camera();

	void projection(float fovy, float aspect, float znear, float zfar);
	void lookat(const glm::vec4& eye, const glm::vec4& look_at, const glm::vec4& up);

	glm::mat4 view_matrix();
	const glm::mat4& proj_matrix() const {return m_projmatrix;}

	float fov() const {return m_fov;}
	float aspect() const {return m_aspect;}
	float znear() const {return m_znear;}
	float zfar() const {return m_zfar;}

	/*void move_up() */

protected:
	virtual void update_matrix();

	float m_fov;
	float m_aspect; 
	float m_znear;
	float m_zfar;

	glm::vec3 m_up;
	glm::vec3 m_look_at;

	bool m_is_proj_dirty;

	glm::mat4 m_projmatrix;
	glm::mat4 m_viewmatrix;	
};



class Model : public Actor{	
	Model() {}
public:
	enum ModelType{
		Box, 
		Sphere,
		Cylinder, 
		Teapot,
		Unknown
	};

	static 	std::shared_ptr<Model> CreateBox(float width = 1, float height = 1, float depth = 1);
	static std::shared_ptr<Model> CreateCylinder(float radius1 = 1, float radius2 = 1, float length = 1, size_t slices = 8, size_t stacks = 8);
	static std::shared_ptr<Model> CreateTeapot();
	static std::shared_ptr<Model> CreateSphere(float radius = 1, size_t slices = 8, size_t stacks = 8);

	static std::shared_ptr<Model> Create(mesh_ptr mesh, gpu_program_ptr shader);
	~Model() {}

	void update() {}
	void render(const glm::mat4& view, const glm::mat4& proj);

	mesh_ptr m_mesh;
	gpu_program_ptr shader;
	ModelType type;
};


class Scene {
public:
	Scene() : m_root("root") {}

	virtual bool quere(Camera& /*cam*/, std::vector<entity_ptr>& /*objs*/, size_t /*flags*/) {return false;}

protected:
	Entity m_root;
};