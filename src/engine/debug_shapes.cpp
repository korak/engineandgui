#include "stdafx.h"
#include "debug_shapes.h"

const char vs[] = "#version 420 core\n"
	"\n"
	"uniform mat4 mvp;\n"
	"uniform vec4 mat_color;\n"
	"\n"
	"#define ATTR_POSITION	0\n"
	"#define ATTR_COLOR		1\n"
	"\n"
	"layout(location = ATTR_POSITION) in vec4 Position;\n"
	"layout(location = ATTR_COLOR) in vec4 Color;\n"
	"\n"
	"out vec4 FragColor;\n"
	"\n"
	"void main(){\n"
	"	gl_Position = mvp * Position;\n"
	"	FragColor =  Color*mat_color;\n"
	"}\n";


const char fs[] = "#version 420 core\n"
	"\n"
	"#define FRAG_COLOR		0\n"
	"\n"
	"layout(location = FRAG_COLOR, index = 0) out vec4 Color;\n"
	"\n"
	"in vec4 FragColor;\n"
	"\n"
	"void main(){\n"
	"	Color = FragColor;\n"
	"}\n";


const glm::vec4 debug_shapes::default_color  {1};

debug_shapes::debug_shapes() {
	m_shader = GpuProgram::create(vs, fs);

	vertex_atrib va[] = {
		{0, 4, GL_FLOAT, false, sizeof(point_t), 0},
		{1, 4, GL_UNSIGNED_BYTE, true, sizeof(point_t), sizeof(glm::vec4)},
		{-1}
	};

	m_grid_mesh = Mesh::create(va);
	m_cone_mesh = Mesh::create(va);
	m_sphere_mesh = Mesh::create(va);
	m_axis_mesh = Mesh::create(va);
	m_cylinder_mesh = Mesh::create(va);

	using namespace glm;
	{	// box		
		point_t points[8] = {
			vec4(-1, -0.5f, -1, 1.0f), 0xFFFFFFFF,
			vec4(1, -0.5f, -1, 1.0f), 0xFFFFFFFF,
			vec4(1, -0.5f, 1, 1.0f), 0xFFFFFFFF,
			vec4(-1, -0.5f, 1, 1.0f), 0xFFFFFFFF,
			vec4(-1, 0.5f, -1, 1.0f), 0xFFFFFFFF,
			vec4(1, 0.5f, -1, 1.0f), 0xFFFFFFFF,
			vec4(1, 0.5f, 1, 1.0f), 0xFFFFFFFF,
			vec4(-1, 0.5f, 1, 1.0f), 0xFFFFFFFF
		};

		unsigned short ib_data[] = {
			0, 1, 3,	1, 2, 3,
			0, 4, 5,	5, 1, 0,
			1, 5, 6,	6, 2, 1,
			2, 6, 7,	7, 3, 2,
			3, 7, 4,	4, 0, 3,
			4, 5, 7,	5, 6, 7,
		};


		m_box_mesh = Mesh::create(va);
		m_box_mesh->update_ib(sizeof(ib_data), ib_data);
		m_box_mesh->update_vb(sizeof(points), points);
	}

}

debug_shapes::~debug_shapes() {

}

void debug_shapes::draw_cone(const glm::mat4& matrix, const glm::vec4& color) {
	
}

void debug_shapes::draw_cylinder(const glm::mat4& matrix, const glm::vec4& color) {

}

void debug_shapes::draw_box(const glm::mat4& matrix, const glm::vec4& color) {
	const glm::mat4 vp = m_proj*(m_view*matrix);
	
	m_shader->begin();
	m_shader->set("mvp",vp);
	m_shader->set("mat_color",color);
		m_box_mesh->draw(Mesh::Triangles);
	m_shader->end();
}

void debug_shapes::draw_frustm(const glm::mat4& matrix, const glm::vec4& color) {

}

void debug_shapes::draw_sphere(const glm::mat4& matrix, const glm::vec4& color) {

}

void debug_shapes::setup(const glm::mat4& view, const glm::mat4& proj) {
	m_view = view;
	m_proj = proj;
	m_view_proj = proj * view;
}