#include "stdafx.h"
#include "opengl.h"
#include "LineDrawer.h"

static const char line3d_vs[] = "#version 420 core\n"
				"\n"
				"uniform mat4 mvp;\n"
				"uniform vec4 mat_color;\n"
				"\n"
				"#define ATTR_POSITION	0\n"
				"#define ATTR_COLOR		1\n"
				"\n"
				"layout(location = ATTR_POSITION) in vec4 Position;\n"
				"layout(location = ATTR_COLOR) in vec4 Color;\n"
				"\n"
				"out vec4 FragColor;\n"
				"\n"
				"void main(){\n"
				"	gl_Position = mvp * Position;\n"
				"	FragColor =  Color;//*mat_color;\n"
				"}\n";

static const char line3d_fs[] = "#version 420 core\n"
				"\n"
				"#define FRAG_COLOR		0\n"
				"\n"
				"layout(location = FRAG_COLOR, index = 0) out vec4 Color;\n"
				"\n"
				"in vec4 FragColor;\n"
				"\n"
				"void main(){\n"
				"	Color = FragColor;\n"
				"}\n";


namespace utility
{
	Canvas::Canvas()
		: m_reserved_size(0)
		, shader(GpuProgram::create(line3d_vs, line3d_fs))
	{

		static vertex_atrib line3d_va[] = {
				{ 0, 3, GL_FLOAT, false, sizeof(vertex_t), 0 },
				{ 1, 4, GL_UNSIGNED_BYTE, true, sizeof(vertex_t), sizeof(glm::vec3) },
				{ -1 }
		};

		m_mesh = Mesh::create(line3d_va);
	}
	
	Canvas::~Canvas()
	{
	}

	void Canvas::render2d(float x, float y, float w, float h)
	{
		update();
		if (m_reserved_size == 0 || m_vertices.empty()) return;

		glDisable(GL_CULL_FACE);
		glDepthMask(GL_FALSE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glm::mat4 mLVP = glm::ortho<float>(x, x+w, y+h, y, -100, 100);

		GLenum errcode0 = glGetError();

		shader->begin();
		shader->set("mvp", mLVP);
		shader->set("mat_color", glm::vec4(1, 1, 1, 1));
		m_mesh->bind();

		for (size_t i = 0; i < batches.size(); ++i) {
			switch (batches[i].mode) {

			case FILL:
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				break;
			case NONE:
			default:
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			}

			glDrawElementsBaseVertex(GL_TRIANGLES, batches[i].num, GL_UNSIGNED_SHORT, 0, batches[i].start);
		}

		m_mesh->unbind();

		shader->end();
		glDisable(GL_BLEND);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glDepthMask(GL_TRUE);
	}

	void Canvas::render(const glm::mat4& mView, const glm::mat4& mProj)
	{
		update();
		if( m_reserved_size == 0 || m_vertices.empty()) return;

		glDisable(GL_CULL_FACE);
		glDepthMask(GL_FALSE);
		glEnable (GL_BLEND);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable (GL_DEPTH_TEST);
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glm::mat4 mLVP =  mProj * mView;			

		GLenum errcode0 = glGetError();
				
		shader->begin();
		shader->set("mvp",mLVP);
		shader->set("mat_color",glm::vec4(1, 1, 1, 1));
		m_mesh->bind();
		
		for (size_t i = 0; i < batches.size(); ++i) {
			switch(batches[i].mode) {
			
			case FILL:
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				break;
			case NONE:
			default:
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			}

			glDrawElementsBaseVertex (GL_TRIANGLES, batches[i].num, GL_UNSIGNED_SHORT, 0, batches[i].start);
		}

		
		//glDrawElements(GL_TRIANGLES, m_vertices.size(), GL_UNSIGNED_SHORT, 0);
		m_mesh->unbind();

		shader->end();	
		glDisable (GL_BLEND);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


		glDepthMask(GL_TRUE);
	}
	//--------------------------------------------------------------------------------------
	void Canvas::clean() {
		// clean place for next frame data :)
		m_vertices.clear();
		batches.clear();
	}
	//--------------------------------------------------------------------------------------
	void Canvas::update()
	{
		if( m_vertices.empty() )  return;
		
		const size_t num_lines = m_vertices.size()/3;
		// if reserver size less - then increase index buffer
		if( num_lines > m_reserved_size )
		{
			m_reserved_size = num_lines;

			const size_t num_indicies = m_vertices.size();
			unsigned short *ib_data = new unsigned short[num_indicies];
			// fill index buffer
			unsigned short *p = ib_data;
			for (size_t i = 0; i < num_indicies; i+=3)
			{				
				*p = (unsigned short)(i); ++p;
				*p = (unsigned short)(i + 1); ++p;
				*p = (unsigned short)(i + 2); ++p;
			}

			m_mesh->update_ib(num_indicies * sizeof(unsigned short), ib_data);			
			delete [] ib_data;
		}

		m_mesh->update_vb(m_vertices.size() * sizeof(vertex_t), &m_vertices[0]);
	}

	void Canvas::add(const glm::vec3& v1, const glm::vec3& v2, const glm::vec3& v3, unsigned long color, fill_mode mode) {
		if (mode == OUTLINE) {
			unsigned int tcolor = (color & 0x00FFFFFF) | 0x29000000;
			add(v1, v2, v3, tcolor, FILL);
			add(v1, v2, v3, color, NONE);
			return;
		}

		if (batches.empty() || batches.back().mode != mode) {
			batch_t new_batch = {m_vertices.size(), 0, mode};
			batches.push_back(new_batch);
		}

		vertex_t v[]  = {
			{v1, color},
			{v2, color},
			{v3, color}
		};
		m_vertices.push_back( v[0]);
		m_vertices.push_back( v[1]);
		m_vertices.push_back( v[2]);

		batches.back().num += 3;
	}

	void Canvas::cube(glm::vec3 offset, float scale, unsigned int color, Canvas::fill_mode mode) {
		const glm::vec3 v[4 * 2] = {
			offset + scale*glm::vec3(-1, -1, -1),	// 0
			offset + scale*glm::vec3(-1, 1, -1),	//1
			offset + scale*glm::vec3(1, 1, -1),		//2
			offset + scale*glm::vec3(1, -1, -1),	//3
			offset + scale*glm::vec3(-1, -1, 1),	//4
			offset + scale*glm::vec3(-1, 1, 1),	//5
			offset + scale*glm::vec3(1, 1, 1),		//6
			offset + scale*glm::vec3(1, -1, 1)		//7
		};

		// left
		add(v[0], v[1], v[2], color, mode);
		add(v[2], v[3], v[0], color, mode);
		//// right
		add(v[4], v[5], v[6], color, mode);
		add(v[6], v[7], v[4], color, mode);

		////front
		add(v[0], v[1], v[5], color, mode);
		add(v[5], v[4], v[0], color, mode);
		////back
		add(v[3], v[2], v[6], color, mode);
		add(v[6], v[7], v[3], color, mode);

		// down
		add(v[4], v[0], v[3], color, mode);
		add(v[3], v[7], v[4], color, mode);
		//up
		add(v[5], v[1], v[2], color, mode);
		add(v[2], v[6], v[5], color, mode);
	}

	void Canvas::cone(glm::vec3 org, glm::vec3 r, glm::vec3 l, unsigned int color, fill_mode mode) {
		glm::vec3 ry = glm::cross(r, l);
		ry = glm::normalize(ry);
		glm::vec3 rx = glm::normalize(r);

		enum {
			NUM_POINTS = 360
		};

		glm::vec3 p[NUM_POINTS + 1];

		for (size_t i = 0; i <= NUM_POINTS; ++i) {
			float rad = glm::radians((float)i);
			p[i] = org + l + (rx*cosf(rad) + ry*sinf(rad))*glm::length(r);
		}

		for (size_t i = 0; i < NUM_POINTS; ++i) {
			add(org, p[i], p[i + 1], color, mode);
		}
	}

	void Canvas::circle(glm::vec3 orig, glm::vec3 rx, glm::vec3 ry, float start, float end, unsigned int color, fill_mode mode) {
		using namespace glm;
		float r = length(rx);
		ry = normalize(ry);
		rx = normalize(rx);

		enum {
			NUM_POINTS = 120
		};

		glm::vec3 p[NUM_POINTS + 1];
		const float angle_step = 360 / NUM_POINTS;

		for (size_t i = 0; i <= NUM_POINTS; ++i) {
			float rad = glm::radians((float)i*angle_step);
			p[i] = orig + (rx*cosf(rad) + ry*sinf(rad))*r;
		}

		for (size_t i = 0; i < NUM_POINTS; ++i) {
			add(p[i], p[i + 1], color);
		}

		size_t start_point = start / angle_step;
		size_t end_point = end / angle_step;

		add(orig, p[start_point], color);
		add(orig, p[end_point], color);

		for (size_t i = start_point; i < end_point; ++i) {
			unsigned int tcolor = (color & 0x00FFFFFF) | 0x33000000;
			add(orig, p[i], p[i + 1], tcolor, utility::Canvas::FILL);
		}
	}

	void Canvas::axis3d()
	{
		const float len = 2.0f;

		const float size_box = 0.05f;//len / 20;

		glm::vec3 org(0);// (0,0,0)
		glm::vec3 X(-len, .0f, .0f);
		glm::vec3 Y(.0f, len, .0f);
		glm::vec3 Z(.0f, .0f, len);

		add(org + X / 4.0f, X, 0xFF0000FF);
		add(org + Y / 4.0f, Y, 0xFF00FF00);
		add(org + Z / 4.0f, Z, 0xFFFF0000);

		const unsigned int yellow = 0xFF00FFFF;

		const float cone_length = 0.26f;
		const float cone_radius = 0.04f;

		// Arrows
		cone(X + X*cone_length, Y*cone_radius, -X*cone_length, 0xFF0000FF);
		cone(Y + Y*cone_length, X*cone_radius, -Y*cone_length, 0xFF00FF00);
		cone(Z + Z*cone_length, X*cone_radius, -Z*cone_length, 0xFFFF0000);
		circle(glm::vec3(0), X, Z, 50, 120, 0xFF00FF00, FILL);
	}

	void Canvas::point(const glm::vec3& p) {
		unsigned int color = 0x25AAFFAA;
		glm::vec3 center = p;//(m_cursor_pos, 1);
		const float size = 3.0f;

		const glm::vec3 Y(0, 1, 1);
		const glm::vec3 X(1, 0, 1);

		add(center + size*Y, center + size*X, color);
		add(center + size*Y, center - size*X, color);

		add(center - size*Y, center + size*X, color);
		add(center - size*Y, center - size*X, color);

		add(center + size*X, center + size*Y, color);
		add(center + size*X, center - size*Y, color);

		add(center - size*X, center + size*Y, color);
		add(center - size*X, center - size*Y, color);
	}

	void Canvas::grid3d(float size, float step) {
		//const float step = 0.25f;
		//const float size = 10.0f;
		const float half_size = size / 2.0f;

		const float step_dark = step * 4;

		for (float i = -half_size; i < half_size + step; i += step)
		{
			add(glm::vec3(-half_size, 0, i), glm::vec3(half_size, 0, i), 0xFFAAAAAA);
			add(glm::vec3(i, 0, -half_size), glm::vec3(i, 0, half_size), 0xFFAAAAAA);
		}

		for (float i = -half_size; i < half_size + step_dark; i += step_dark)
		{
			add(glm::vec3(-half_size, 0, i), glm::vec3(half_size, 0, i), 0xFF080808);
			add(glm::vec3(i, 0, -half_size), glm::vec3(i, 0, half_size), 0xFF080808);
		}
	}

	void Canvas::grid2d(float size, float step) {
		//const float step = 10.f;
		//const float size = 800.0f;
		//const float half_size = size / 2.0f;

		const float step_dark = step * 4;

		for (float i = 0; i < size + step; i += step)
		{
			add(glm::vec3(-0, i, 10), glm::vec3(size, i, 10), 0xFFAAAAAA);
			add(glm::vec3(i, -0, 10), glm::vec3(i, size, 10), 0xFFAAAAAA);
		}

		for (float i = 0; i < size + step_dark; i += step_dark)
		{
			add(glm::vec3(-0, i, 0), glm::vec3(size, i, 0), 0xFF080808);
			add(glm::vec3(i, -0, 0), glm::vec3(i, size, 0), 0xFF080808);
		}
	}
} //~ namespace utility