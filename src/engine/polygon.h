#pragma once

void MakePolygon( vertex* pVertices, unsigned short* pwIndices, float fLength, size_t uSides )
{
	// Calculate the radius
	float radius = fLength * 0.5f / sinf( PI / ( float )uSides );
	float angle = ( float )( 2.0f * PI / ( float )uSides );

	// Fill in vertices
	vertex* pVertex = pVertices;

	pVertex->pos = glm::vec3( 0.0f, 0.0f, 0.0f );
	pVertex->norm = glm::vec3( 0.0f, 0.0f, 1.0f );
	pVertex++;

	for( size_t j = 0; j < uSides; j++ )
	{
		float s, c;
		sincosf( angle * j, &s, &c );

		pVertex->pos = glm::vec3( c * radius, s * radius, 0.0f );
		pVertex->norm = glm::vec3( 0.0f, 0.0f, 1.0f );
		pVertex++;
	}

	// Fill in indices
	unsigned short* pwFace = pwIndices;

	size_t iFace = 0;
	for( iFace = 0; iFace < uSides - 1; iFace++ )
	{
		pwFace[0] = 0;
		pwFace[1] = iFace + 1;
		pwFace[2] = iFace + 2;

		pwFace += 3;
	}

	// handle the wrapping of the last case
	pwFace[0] = 0;
	pwFace[1] = iFace + 1;
	pwFace[2] = 1;
}


//----------------------------------------------------------------------------
// DXUTCreatePolygon - create a polygon mesh
//----------------------------------------------------------------------------
mesh_ptr create_polygon(float fLength, size_t uSides)
{
	unsigned short* pwIndices = NULL;
	vertex* pVertices = NULL;

	//// Set up the defaults
	//if( D3DX_DEFAULT == uSides )
	//	uSides = 3;
	//if( D3DX_DEFAULT_FLOAT == fLength )
	//	fLength = 1.0f;


	// Validate parameters
	if( fLength < 0.0f )
		return mesh_ptr();
	if( uSides < 3 )
		return mesh_ptr();

	// Create the mesh
	const size_t cFaces = uSides;
	const size_t cVertices = uSides + 1;

	// Create enough memory for the vertices and indices
	pVertices = new vertex[ cVertices ];
	if( !pVertices )
		return mesh_ptr();
	pwIndices = new unsigned short[ cFaces * 3 ];
	if( !pwIndices ) {
		delete [] pVertices;
		return  mesh_ptr();
	}

	// Create a polygon
	MakePolygon( pVertices, pwIndices, fLength, uSides );

	// Create a mesh
	mesh_ptr mesh = create_mesh(pVertices, cVertices, pwIndices, cFaces * 3 );

	// Free up the memory
	delete [] pVertices;
	delete [] pwIndices;

	return mesh;
}

