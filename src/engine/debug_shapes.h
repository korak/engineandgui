#pragma once
#include "render_gl.h"

class debug_shapes {
public:
	struct point_t {
		glm::vec4		pos;
		unsigned long	color;
	};

	static const glm::vec4 default_color;

	debug_shapes();
	~debug_shapes();

	void setup(const glm::mat4& view, const glm::mat4& proj);

	void draw_axis(const glm::mat4& matrix, const glm::vec4& color = glm::vec4(1));
	void draw_cone(const glm::mat4& matrix, const glm::vec4& color = glm::vec4(1));
	void draw_cylinder(const glm::mat4& matrix, const glm::vec4& color = glm::vec4(1));
	void draw_box(const glm::mat4& matrix, const glm::vec4& color = glm::vec4(1));
	void draw_frustm(const glm::mat4& matrix, const glm::vec4& color = glm::vec4(1));
	void draw_sphere(const glm::mat4& matrix, const glm::vec4& color = glm::vec4(1));
	void draw_grid(const glm::mat4& matrix, const glm::vec4& color = glm::vec4(1));

private:
	mesh_ptr m_box_mesh;
	mesh_ptr m_grid_mesh;
	mesh_ptr m_cone_mesh;
	mesh_ptr m_sphere_mesh;
	mesh_ptr m_axis_mesh;
	mesh_ptr m_cylinder_mesh;
	
	gpu_program_ptr m_shader;

	glm::mat4 m_view;
	glm::mat4 m_proj;
	glm::mat4 m_view_proj;
};