#include "stdafx.h"

#include "engine.h"

const static char default_vs[] = "#version 410 core\n"
"uniform mat4 mvp;\n"
"uniform vec4 diffuse_color = vec4(1,1,1,1);\n"
"uniform vec4 light_dir = vec4(1,2,0.5f,1);\n"
"#define ATTR_POSITION	0\n"
"#define ATTR_NORMAL	1\n"
"layout(location = ATTR_POSITION) in vec4 Position;\n"
"layout(location = ATTR_NORMAL) in vec4 Normal;\n"
"out vec4 FragColor;\n"
"void main(){\n"
"	vec4 ldir_norm = normalize(light_dir);\n"
"	gl_Position = mvp * Position;\n"
"	FragColor =  diffuse_color*0.5f + 0.5f*dot(Normal,ldir_norm);\n"
"}\n";

const static char default_fs[] = "#version 410 core\n"
"#define FRAG_COLOR		0\n"
"layout(location = FRAG_COLOR, index = 0) out vec4 Color;\n"
"in vec4 FragColor;\n"
"void main(){\n"
"	Color = FragColor;\n"
"}\n";

void Entity::update_matrix() {
	glm::mat4 t = glm::translate(m_position);
	glm::mat4 s = glm::scale(m_scale);
	glm::mat4 r = glm::mat4_cast(m_rotation);
	m_local_transform = r*t;//*r;//*s;
	is_dirty = false;
}


#include "shapes.h"

std::shared_ptr<Model> Model::Create(mesh_ptr mesh, gpu_program_ptr shader) {
	std::shared_ptr<Model> model(new Model);
	model->m_mesh = mesh;
	model->shader = shader;
	model->type = Unknown;
	return model;
}


std::shared_ptr<Model> Model::CreateBox(float w, float h, float d) {
	auto mesh = shapes::create_box(w, h, d);
	auto shader = GpuProgram::create(default_vs, default_fs);
	auto model = Model::Create(mesh, shader);

	static char name_buff[128];
	static int shapes_counter = 0;
	sprintf(name_buff, "box%.3d", ++shapes_counter);
	model->name(name_buff);
	return model;
}

std::shared_ptr<Model> Model::CreateCylinder(float radius1, float radius2, float length, size_t slices, size_t stacks) {
	auto mesh = shapes::create_cylinder(radius1, radius2, length, slices, stacks);
	auto shader = GpuProgram::create(default_vs, default_fs);
	auto model = Model::Create(mesh, shader);

	static char name_buff[128];
	static int shapes_counter = 0;
	sprintf(name_buff, "cylinder%.3d", ++shapes_counter);
	model->name(name_buff);
	return model;
}

std::shared_ptr<Model> Model::CreateTeapot() {
	auto mesh = shapes::create_teapot();
	auto shader = GpuProgram::create(default_vs, default_fs);
	auto model = Model::Create(mesh, shader);

	static char name_buff[128];
	static int shapes_counter = 0;
	sprintf(name_buff, "teapot%.3d", ++shapes_counter);
	model->name(name_buff);
	return model;
}

std::shared_ptr<Model> Model::CreateSphere(float radius, size_t slices, size_t stacks) {
	auto mesh = shapes::create_sphere(radius, slices, stacks);
	auto shader = GpuProgram::create(default_vs, default_fs);
	auto model = Model::Create(mesh, shader);

	static char name_buff[128];
	static int shapes_counter = 0;
	sprintf(name_buff, "sphere%.3d", ++shapes_counter);
	model->name(name_buff);
	return model;
}



void Model::render(const glm::mat4& view, const glm::mat4& proj) {
	glDisable(GL_BLEND);
	glDisable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	shader->begin();
	m_mesh->bind();

	if (is_dirty)
		update_matrix();

	const glm::mat4 vp = proj*(view*m_local_transform);

	//shader->set("mvp", trasform());
	shader->set("mvp", vp);
	shader->set("diffuse_color", glm::vec4(1, 1, 0, 1));

	//glDrawElements (GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, 0);
	size_t num_triangles = m_mesh->ib->size/ sizeof(unsigned short);
	glDrawElements (GL_TRIANGLES, num_triangles, GL_UNSIGNED_SHORT, 0);

	m_mesh->unbind();

	shader->end();
}