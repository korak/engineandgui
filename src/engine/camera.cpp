#include "stdafx.h"

#include "engine.h"


Camera::Camera() 
	: m_projmatrix(1.0f), 
	m_viewmatrix(1.0f)
{
	m_type_id = E_CAMERA;
}

void Camera::projection(float fovy, float aspect, float znear, float zfar)
{
	m_fov = fovy;
	m_aspect = aspect; 
	m_znear = znear;
	m_zfar = zfar;
}

void Camera::lookat(const glm::vec4& eye_pt, const glm::vec4& lookat_pt, const glm::vec4& up_pt)
{
	m_look_at = glm::vec3(lookat_pt);
	m_position = glm::vec3(eye_pt);
	m_up = glm::vec3(up_pt);
	m_scale = glm::vec3(1);

	glm::vec3 look_dir = glm::normalize(m_look_at - m_position);	
	m_viewmatrix = glm::lookAt(m_position, m_look_at, m_up);
	m_local_transform = glm::inverse(m_viewmatrix);
	m_rotation = glm::quat(m_local_transform);

	is_dirty = false;
}

glm::mat4 Camera::view_matrix() 
{
	update_matrix();
	//glm::vec4 cam_pos = m_position;

	//glm::quat qv (glm::vec3(0,0,m_roty));
	//glm::quat qh (glm::vec3(0,m_rotx,0));
	//glm::quat q = qv * qh;

	//glm::mat4 rotation = glm::mat4_cast(q);
	//cam_pos = cam_pos * rotation;

	//m_viewmatrix = glm::lookAt(m_position, glm::vec3(0), glm::vec3(0,1,0));

	//glm::lookAt

	return m_viewmatrix;
}



void Camera::update_matrix() {

	if (is_dirty) {
		Entity::update_matrix();
		m_viewmatrix = glm::inverse(m_local_transform);
	}

	if (m_is_proj_dirty) {
		m_projmatrix = glm::perspective(m_fov, m_aspect, m_znear, m_zfar);
		m_is_proj_dirty = false;
	}
}

